#!/usr/bin/env sh
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm install airflow-stable stable/airflow -f rzd-values.yaml
