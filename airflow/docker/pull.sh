#!/bin/bash
cd /usr/local/airflow/rzd || exit
if git fetch origin master &&
    [ `git rev-list HEAD...origin/master --count` != 0 ]
then
    echo 'Need pull.'
    git pull
    cp -R /usr/local/airflow/rzd/dags/* /usr/local/airflow/dags
else
    echo 'Do nothind: up-to-date.'
fi
