# Docker
Для того, чтобы использовать airflow в контейнере, начала нужно собрать образ, для этого нужно перейти в папку `docker` и выполнить команду
```shell_script
docker build -t airflow:local .
```

# Docker compose
Для запуска aiflow в докере локально нужно перейти в папку `docker` и выполнить команды:
```shell_script
docker-compose up -d airflow
```
После этого airflow будет доступен по порту 8080, а используемый им posgress по порту 5432.

# Kubernetes
Перейти в папку `helm` и выполнить команду 
```shell_script
kubectl create secret docker-registry ligaregcred --docker-server=gitlab.akb-it.ru:4567/rnd/rzd --docker-username=<phoenix_user> --docker-password
=<phoenix_password> --docker-email=<phoenix-user>@phoenixit.ru
helm install <имя> .
kubectl port-forward svc/<имя>-airflow-web 7000:80
```
После этого все задачи airflow будут выполняться в отдельных подах.