# rzd

## Ресурсы проекта
|Тип    |               |hostname                           |           |Диски, Гб (с учетом RAID)  |плюс под ОС, Гб    |Оперативная память, Гб |Ядра, шт   |IP             |
| ---   | ------------- | --------------------------------- | --------- | ------------------------- | ----------------- | --------------------- | --------- | ------------- |
|Вирт   |Управляющий    |(Ambari, ADCM, Monitoring, Zabbix) |RAID 1     |960                        |                   |32                     |8          |10.20.121.24   |
|Вирт   |Hadoop         |namenode                           |RAID 1     |960                        |                   |96                     |12         |10.20.121.23   |
|Вирт   |               |datanode1                          |JBOD       |1920                       |                   |128                    |16         |10.20.121.20   |
|Вирт   |               |datanode2                          |JBOD       |1920                       |                   |128                    |16         |10.20.121.21   |
|Вирт   |               |datanode3                          |JBOD       |1920                       |                   |128                    |16         |10.20.121.22   |
|Вирт   |ETL            |ETL1                               |RAID 1     |960                        |                   |32                     |8          |10.20.121.16   |
|Вирт   |               |ETL2                               |RAID 1     |960                        |                   |32                     |8          |10.20.121.18   |
|Вирт   |               |ETL3                               |RAID 1     |960                        |                   |32                     |8          |10.20.121.19   |
|Вирт   |ADB            |master                             |RAID 1     |960                        |                   |128                    |12         |10.20.121.17   |
|Физ    |               |segment1                           |RAID 10    |5760                       |                   |320                    |32         |10.20.121.14   |
|Физ    |               |segment2                           |RAID 10    |5760                       |                   |320                    |32         |10.20.121.13   |


## Настройка окружения

### VPN

[Инструкция по настройке](Способ_подключения_к_ДЕМО_стенду.docx)
Клиент доступен на оффициальном сайте (6-ая версия):
* [Windows](https://filestore.fortinet.com/forticlient/downloads/FortiClientOnlineInstaller_6.0.exe​)
* [Не Windows](https://www.forticlient.com/downloads)

### Kubernetes
Развертывание провдено по [данной инструкции](https://www.linuxtechi.com/install-kubernetes-1-7-centos7-rhel7/)
Дополнительно отключен swap на всех узлах:
```
swapoff -a
vim /etc/fstab -> закоментировать раздел со swap
```
Кубернетес установлен на следующие узлы
```
10.20.121.16 k8s-master
10.20.121.18 k8s-workernode1
10.20.121.19 k8s-workernode2
```

#### Удаленное подключение с помощью kubectl
С узла `k8s-master` перенсети записи (или файл целиком) из `config` в такой же нас своем компьютере.
На текущий момент эта конфигурация такая:
```yaml
clusters:
- cluster:
    server: https://10.20.121.16:6443
  name: arenadata
contexts:
- context:
    cluster: arenadata
    user: arenadata
  name: arenadata
current-context: arenadata
users:
- name: arenadata
  user:
```

### Aiflow
Развертывается по [этой инструкции](https://gitlab.akb-it.ru/rnd/sber-hr/tree/master/airflow).

#### Подключение к Airflow
Ввиду того что не развернуты никакие балансировщики и прокси, для доступа к сервису нужно настроить удаленное подключение с помощью kubectl и выполнить команду
```
# В браузере доступ по адресу 
kubectl port-forward svc/airflow-web 7000:80 
```

#### Подключение к GreenPlum
Строка подключения: jdbc:postgresql://10.20.121.17:5432/postgres 
Логин:adb_admin
Пароль:adb_admin


#### Подключение к Hive
Строка подключения: jdbc:hive2://10.20.121.23:10000/default
Логин:пусто
Пароль:пусто
