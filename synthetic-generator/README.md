# Генератор синтетических данных

## Требования
1. Java 11+

## Сбока
Дистрибутив создатеся командой
```shell script
./gradlew bootDistZip
```
в директории `build/distributions`

## Использование
Требуется распаковать дистрибутив и запустить файл `bin/synthetic-generator`.
Информация об использовании программы доступна в подсказках (комманда `help` в программе).