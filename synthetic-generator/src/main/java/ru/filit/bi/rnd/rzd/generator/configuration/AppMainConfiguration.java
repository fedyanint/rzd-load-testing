package ru.filit.bi.rnd.rzd.generator.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.filit.bi.rnd.rzd.generator.bean.DictionaryInMemoryRepository;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.BaseKeyValueType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.CargoType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DataType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DateType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.MetricType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.OrganizationType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.UnitType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.ValueType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.VarType;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvailableDate;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvailableSource;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvaliableLogin;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvaliableReason;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:application.yaml")
public class AppMainConfiguration {

    @Bean
    public DictionaryInMemoryRepository dictionaryInMemoryRepository(
            @Value("${app.dict.baseDir}") String baseDictDir,
            @Value("${app.gen.baseDir}") String baseGeneratorDir
                                                                    ) {
        Map<Class<? extends BaseKeyValueType>, Path> dictsSources = new HashMap<>();
        dictsSources.put(CargoType.class, Paths.get(baseDictDir, "skim_cargo_type_t.csv"));
        dictsSources.put(DataType.class, Paths.get(baseDictDir, "skim_data_type_t.csv"));
        dictsSources.put(DateType.class, Paths.get(baseDictDir, "skim_date_type.csv"));
        dictsSources.put(MetricType.class, Paths.get(baseDictDir, "skim_metric_type_t.csv"));
        dictsSources.put(OrganizationType.class, Paths.get(baseDictDir, "skim_org_t.csv"));
        dictsSources.put(UnitType.class, Paths.get(baseDictDir, "skim_unit_t.csv"));
        dictsSources.put(ValueType.class, Paths.get(baseDictDir, "skim_val_type_t.csv"));
        dictsSources.put(VarType.class, Paths.get(baseDictDir, "skim_var_t.csv"));
        dictsSources.put(AvailableDate.class, Paths.get(baseGeneratorDir, "date.csv"));
        dictsSources.put(AvailableSource.class, Paths.get(baseGeneratorDir, "source.csv"));
        dictsSources.put(AvaliableLogin.class, Paths.get(baseGeneratorDir, "login.csv"));
        dictsSources.put(AvaliableReason.class, Paths.get(baseGeneratorDir, "reason.csv"));
        return new DictionaryInMemoryRepository(dictsSources);
    }
}
