package ru.filit.bi.rnd.rzd.generator.service;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jooq.lambda.Unchecked;
import org.springframework.stereotype.Service;
import ru.filit.bi.rnd.rzd.generator.bean.DictionaryInMemoryRepository;
import ru.filit.bi.rnd.rzd.generator.domain.Datamart;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.BaseKeyValueType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.CargoType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DataType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DateType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.MetricType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.OrganizationType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.UnitType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.ValueType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.VarType;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvailableDate;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvailableSource;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvaliableLogin;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvaliableReason;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Service
@RequiredArgsConstructor
public class GenerationService {
    private final DictionaryInMemoryRepository repository;

    private final Random random = new Random();

    private final ExecutorService pool = Executors.newCachedThreadPool();

    private final AtomicInteger fileSeq = new AtomicInteger(0);

    public void generate(Integer nFiles, Integer nEntities, Path outputDirectory, Runnable step) {
        var futures = new ArrayList<Future>();
        for (int i = 0; i < nFiles; i++) {
            futures.add(pool.submit(() -> generateFile(nEntities, outputDirectory, step)));
        }
        futures.forEach(Unchecked.consumer(Future::get));
    }

    @SneakyThrows
    public void generateFile(Integer nEntities, Path outputDirectory, Runnable step) {
        var seq = new AtomicLong(0);
        var date = getRandomElementFromDict(AvailableDate.class);
        var filename = String.format("%03d.xml", fileSeq.incrementAndGet());
        if (!Files.exists(outputDirectory)) {
            Files.createDirectories(outputDirectory);
        }
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
        var file = outputDirectory.resolve(filename);
        if (Files.exists(file)) {
            throw new RuntimeException(String.format("Файл с именем %s уже существует, очистите директорию, так как сервис нумерует файлы по-порядку начиная " +
                                                             "с 1, начальное значение задается при старте программы", file.toAbsolutePath().toString()));
        }
        XMLStreamWriter sw = xmlOutputFactory.createXMLStreamWriter(new FileOutputStream(file.toFile()));

        XmlMapper mapper = new XmlMapper(xmlInputFactory);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        sw.writeStartDocument();
        sw.writeStartElement("root");

        for (int i = 0; i < nEntities; i++) {
            var modTime = new Date(date.getDate().getTime() + seq.incrementAndGet());
            var builder = Datamart.builder()
                                  .date(date.getDate())
                                  .munit(getRandomElementFromDict(UnitType.class))
                                  .dataType(getRandomElementFromDict(DataType.class))
                                  .var(getRandomElementFromDict(VarType.class))
                                  .value(Math.abs(random.nextInt()))
                                  .valueType(getRandomElementFromDict(ValueType.class))
                                  .metricType(getRandomElementFromDict(MetricType.class))
                                  .dateType(getRandomElementFromDict(DateType.class))
                                  .source(getRandomElementFromDict(AvailableSource.class).getSource())
                                  .org(getRandomElementFromDict(OrganizationType.class))
                                  .cargoType(getRandomElementFromDict(CargoType.class))
                                  .reasonText(getRandomElementFromDict(AvaliableReason.class).getReason())
                                  .modTime(modTime)
                                  .userLogin(getRandomElementFromDict(AvaliableLogin.class).getLogin());
            mapper.writeValue(sw, builder.build());
            step.run();
        }
        sw.writeEndElement();
        sw.writeEndDocument();
    }

    private <T extends BaseKeyValueType> T getRandomElementFromDict(Class<T> clazz) {
        List<T> dictionaryByType = repository.getDictionaryByType(clazz);
        return dictionaryByType.get(random.nextInt(dictionaryByType.size()));
    }
}
