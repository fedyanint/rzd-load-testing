package ru.filit.bi.rnd.rzd.generator.domain.dictionary;

import lombok.Data;

@Data
public class VarType extends BaseKeyValueType {
    private String var;

    private String name;
}
