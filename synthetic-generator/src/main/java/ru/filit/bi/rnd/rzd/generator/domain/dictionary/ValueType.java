package ru.filit.bi.rnd.rzd.generator.domain.dictionary;

import lombok.Data;

@Data
public class ValueType extends BaseKeyValueType {
    private String id;

    private String name;
}
