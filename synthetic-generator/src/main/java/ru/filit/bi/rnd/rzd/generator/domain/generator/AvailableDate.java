package ru.filit.bi.rnd.rzd.generator.domain.generator;

import lombok.Data;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.BaseKeyValueType;

import java.util.Date;

@Data
public class AvailableDate extends BaseKeyValueType {
    private Date date;
}
