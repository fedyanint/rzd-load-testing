package ru.filit.bi.rnd.rzd.generator.components;

import lombok.RequiredArgsConstructor;
import me.tongfei.progressbar.ProgressBar;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.filit.bi.rnd.rzd.generator.bean.DictionaryInMemoryRepository;
import ru.filit.bi.rnd.rzd.generator.domain.generator.AvailableDate;
import ru.filit.bi.rnd.rzd.generator.service.GenerationService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@ShellComponent
@RequiredArgsConstructor
public class GeneratorCommands {

    private final GenerationService service;

    private final DictionaryInMemoryRepository repository;

    @Value("${app.gen.outputDir}")
    private Path outputDirectory;

    @ShellMethod(
            key = "dir",
            value = "Вывести путь к директории с результатами",
            prefix = ""
    )
    public String getDir() throws IOException {
        var sb = new StringBuilder();
        var path = outputDirectory.toAbsolutePath();
        sb.append("Path: ").append(path.toString());
        var existence = Files.isDirectory(path);
        sb.append("\nСуществование: ").append(existence);
        if (existence) {
            var isClean = Files.list(path).count() == 0;
            sb.append("\nПустая (нет ни файлов, ни других директорий): ").append(isClean);
        }
        return sb.toString();
    }

    @ShellMethod(
            key = "date",
            value = "Переопределить словарь с датами на одну единственную",
            prefix = ""
    )
    public String date(
            @ShellOption(
                    value = "-v",
                    help = "Дата в формате dd.MM.yyyy"
            ) String date
                      ) throws ParseException {
        var dictionaryByType = repository.getDictionaryByType(AvailableDate.class);
        dictionaryByType.clear();
        var newDate = new AvailableDate();
        newDate.setDate(new SimpleDateFormat("dd.MM.yyyy").parse(date));
        dictionaryByType.add(newDate);
        return "Новый словарь доступных дат:\n" + dictionaryByType;
    }

    @ShellMethod(
            key = "clean",
            value = "Очистить содержимое директории с результатми генерации",
            prefix = ""
    )
    public String clean() throws IOException {

        if (!Files.exists(outputDirectory)) {
            return String.format("Директории %s не существует, нечего очищать", outputDirectory);
        }
        FileUtils.cleanDirectory(outputDirectory.toFile());
        return "Ok!";
    }

    @ShellMethod(
            key = "gen",
            value = "Сгенерировать данные",
            prefix = ""
    )
    public String gen(
            @ShellOption(
                    defaultValue = "10",
                    value = "--nf",
                    help = "Количество файлов, которые будут сгенерированы"
            ) Integer nFiles,
            @ShellOption(
                    defaultValue = "100000",
                    value = "--ne",
                    help = "Количество сущностей, которые будут сгенерированы в каждом файл"
            ) Integer nEntities
                     ) {
        try (ProgressBar pb = new ProgressBar("Generation", nEntities * nFiles)) {
            service.generate(nFiles, nEntities, outputDirectory, pb::step);
        }
//        service.generate(nFiles, nEntities, outputDirectory, () -> {
//        });
        return "Завершено";
    }
}
