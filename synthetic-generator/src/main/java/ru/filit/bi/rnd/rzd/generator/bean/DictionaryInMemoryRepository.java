package ru.filit.bi.rnd.rzd.generator.bean;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.beans.factory.InitializingBean;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.BaseKeyValueType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DictionaryInMemoryRepository implements InitializingBean {
    private Map<Class<? extends BaseKeyValueType>, List<? extends BaseKeyValueType>> dicts;

    private Map<Class<? extends BaseKeyValueType>, Path> sources;

    public DictionaryInMemoryRepository(Map<Class<? extends BaseKeyValueType>, Path> sources) {
        dicts = new HashMap<>();
        this.sources = sources;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        sources.forEach((type, path) -> dicts.put(type, loadData(path, type)));
    }

    private List<? extends BaseKeyValueType> loadData(Path path, Class<? extends BaseKeyValueType> type) {
        if (path == null || !Files.exists(path) || !Files.isRegularFile(path)) {
            throw new RuntimeException(String.format("Путь %s не существует или не является реуглярным файлом", path != null ? path.toString() : "<не задан>"));
        }
        return loadObjectSet(type, path.toFile());
    }

    public <T extends BaseKeyValueType> List<T> getDictionaryByType(Class<T> clazz) {
        return (List<T>) dicts.get(clazz);
    }

    public <T> List<T> loadObjectSet(Class<T> type, File file) {
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader().withColumnSeparator(';');
            CsvMapper mapper = new CsvMapper();
            MappingIterator<T> readValues =
                    mapper.readerFor(type).with(bootstrapSchema).readValues(file);
            return readValues.readAll();
        } catch (IOException e) {
            throw new RuntimeException("Не удалось считать словарь из файла " + file, e);
        }
    }
}
