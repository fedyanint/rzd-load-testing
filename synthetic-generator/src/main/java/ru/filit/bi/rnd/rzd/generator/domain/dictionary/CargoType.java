package ru.filit.bi.rnd.rzd.generator.domain.dictionary;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CargoType extends BaseKeyValueType {
    @JsonProperty("cargo_type")
    private String cargoType;

    private String name;
}
