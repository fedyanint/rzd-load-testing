package ru.filit.bi.rnd.rzd.generator.domain.generator;

import lombok.Data;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.BaseKeyValueType;

@Data
public class AvaliableLogin  extends BaseKeyValueType {
    private String login;
}
