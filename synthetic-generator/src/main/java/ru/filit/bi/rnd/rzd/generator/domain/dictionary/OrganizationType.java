package ru.filit.bi.rnd.rzd.generator.domain.dictionary;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrganizationType extends BaseKeyValueType {
    private Integer id;

    private String name;

    private BigDecimal bk;

    private String vName;

    @JsonProperty("root_id")
    private Integer rootId;

    @JsonProperty("child_id")
    private Integer childId;
}
