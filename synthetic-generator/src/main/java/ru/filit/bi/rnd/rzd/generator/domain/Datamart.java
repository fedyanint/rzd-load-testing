package ru.filit.bi.rnd.rzd.generator.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.CargoType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DataType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.DateType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.MetricType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.OrganizationType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.UnitType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.ValueType;
import ru.filit.bi.rnd.rzd.generator.domain.dictionary.VarType;

import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Builder
public class Datamart {
    private Date date;

    @Size(max = 2)
    private String munit;

    @JsonProperty("data_type")
    private String dataType;

    @Size(max = 30)
    private String var;

    private Integer value;

    @JsonProperty("val_type")
    private String valueType;

    @JsonProperty("metric_type")
    @Size(max = 4)
    private String metricType;

    @JsonProperty("date_type")
    private Integer dateType;

    @Size(max = 10)
    private String source;

    private Integer org;

    @JsonProperty("cargo_type")
    private String cargoType;

    @JsonProperty("reason_text")
    @Size(max = 3000)
    private String reasonText;

    @JsonProperty("mod_time")
    private Date modTime;

    @JsonProperty("user_login")
    @Size(max = 50)
    private String userLogin;

    public static class DatamartBuilder {
        public DatamartBuilder var(VarType varType) {
            this.var = varType.getVar();
            return this;
        }

        public DatamartBuilder org(OrganizationType organizationType) {
            this.org = organizationType.getId();
            return this;
        }

        public DatamartBuilder cargoType(CargoType cargoType) {
            this.cargoType = cargoType.getCargoType();
            return this;
        }
        public DatamartBuilder dataType(DataType dataType) {
            this.dataType = dataType.getId();
            return this;
        }

        public DatamartBuilder dateType(DateType dateType) {
            this.dateType = dateType.getId();
            return this;
        }

        public DatamartBuilder metricType(MetricType metricType) {
            this.metricType = metricType.getId();
            return this;
        }

        public DatamartBuilder munit(UnitType unitType) {
            this.munit = unitType.getId();
            return this;
        }

        public DatamartBuilder valueType(ValueType valueType) {
            this.valueType = valueType.getId();
            return this;
        }
    }

}
