add jar /usr/lib/hive/lib/hivexmlserde-1.0.5.3.jar;

--ALTER TABLE ods_skim_datamart ADD IF NOT EXISTS PARTITION (load_id=:P_LOAD_ID);

ALTER TABLE ods_skim_datamart DROP IF EXISTS PARTITION(load_id=4)

INSERT  INTO ods_skim_datamart PARTITION(load_id=4)
SELECT
CURRENT_TIMESTAMP AS load_date,
from_utc_timestamp(cal_date,'MSK') as cal_date,
munit,
var,
value,
source,
org,
data_type,
val_type,
metric_type,
date_type,
cargo_type,
reason_text,
from_utc_timestamp(mod_time,'MSK') as mod_time,
user_login
FROM 
stg_skim_datamart;
