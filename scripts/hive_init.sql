drop table stg_skim_datamart;

CREATE TABLE stg_skim_datamart(
cal_date bigint,
munit string,
var string,
value int,
source string,
org int,
data_type string,
val_type string,
metric_type string,
date_type int,
cargo_type string,
reason_text string,
mod_time bigint,
user_login string
)
ROW FORMAT SERDE 'com.ibm.spss.hive.serde2.xml.XmlSerDe'
WITH SERDEPROPERTIES (
"xml.processor.class" = "com.ximpleware.hive.serde2.xml.vtd.XmlProcessor",
"column.xpath.cal_date"="/Datamart/date/text()",
"column.xpath.munit"="/Datamart/munit/text()",
"column.xpath.var"="/Datamart/var/text()",
"column.xpath.value"="/Datamart/value/text()",
"column.xpath.source"="/Datamart/source/text()",
"column.xpath.org"="/Datamart/org/text()",
"column.xpath.data_type"="/Datamart/data_type/text()",
"column.xpath.val_type"="/Datamart/val_type/text()",
"column.xpath.metric_type"="/Datamart/metric_type/text()",
"column.xpath.date_type"="/Datamart/date_type/text()",
"column.xpath.cargo_type"="/Datamart/cargo_type/text()",
"column.xpath.reason_text"="/Datamart/reason_text/text()",
"column.xpath.mod_time"="/Datamart/mod_time/text()",
"column.xpath.user_login"="/Datamart/user_login/text()"
)
STORED AS
INPUTFORMAT 'com.ibm.spss.hive.serde2.xml.XmlInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
LOCATION '/user/admin/stg_xml'
TBLPROPERTIES (
"xmlinput.start"="<Datamart>",
"xmlinput.end"="</Datamart>"
);

--create
drop table ods_skim_datamart;

CREATE TABLE ods_skim_datamart(
load_date date,
cal_date date,
munit varchar(4000),
var varchar(4000),
value int,
source varchar(4000),
org int,
data_type varchar(4000),
val_type varchar(4000),
metric_type varchar(4000),
date_type int,
cargo_type varchar(4000),
reason_text varchar(4000),
mod_time date,
user_login varchar(4000)
)
PARTITIONED BY (load_id int);

ALTER TABLE ods_skim_datamart ADD IF NOT EXISTS PARTITION (load_id=0);
