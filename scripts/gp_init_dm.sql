/* Создание необходимых таблиц в GP и импорт данных */

-- create extension pxf;
-- drop external table ods_skim_datamart
create external table 
ods_skim_datamart (load_date timestamp, cal_date timestamp, munit varchar(4000), var varchar(4000), value integer, 
                   source varchar(4000), org integer, data_type varchar(4000), val_type varchar(4000), 
                   metric_type varchar(4000), date_type integer, cargo_type varchar(4000), reason_text varchar(4000),
                   mod_time timestamp, user_login varchar(4000), load_id integer)
location ('pxf://default.ods_skim_datamart?PROFILE=Hive')
format 'custom' (FORMATTER='pxfwritable_import');

--drop table public.dm_all_indicators_v
create table public.dm_all_indicators_v  
(like public.ods_skim_datamart) 
with (appendonly=true, orientation=column) 
distributed by (org) 
partition by range (load_id) (start (1) end (11) every (1));
alter table public.dm_all_indicators_v 
alter column value type numeric(20,5) using value::numeric(20,5),
add column md5 uuid,
add column hcode_id varchar(5);


-- drop table public.dm_all_indicators_t
create table public.dm_all_indicators_t
(like public.dm_all_indicators_v)
with (appendonly=true, orientation=column)
distributed by (org);