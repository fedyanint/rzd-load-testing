CREATE TABLE stg_skim_cargo_type_t 
(cargo_type VARCHAR(4000),
name VARCHAR(4000));

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('60', 'ЖИВНОСТЬ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1022', 'Размещение объектов связи');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('12', 'ПРОМ.СЫРЬЕ И ФОРМ. МАТ-ЛЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1012', 'Коммунального назначения');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('55', 'БУМАГА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('83', 'СОЛЬ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('87', 'ЦВЕТНЫЕ МЕТАЛЛЫ,ЛОМ ЦВ.МЕТАЛЛОВ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1028', 'Тема обращения ЕИСЦ: Жалобы(претензии),');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('14', 'ЦЕМЕНТ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('46', 'УСТ. ЗАВ. КВАЛ. ТРЕБОВАНИЙ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1002', 'Северо-Западный ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1027', 'Тема обращения ЕИСЦ:  Перевозка грузов в контейнера');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1031', 'Тема обращения ЕИСЦ: Электронный обмен документами');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('15', 'ЛЕСНЫЕ ГРУЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('71', 'ОСТАЛЬНЫЕ И СБОРНЫЕ ГРУЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1001', 'Южный ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('85', 'ФЛЮСЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('7', 'РУДА ЖЕЛЕЗНАЯ И МАРГАНЦЕВАЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1009', 'Здравоохранение');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('other', 'Прочее');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('65', 'КОМБИКОРМА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('81', 'СЕЛЬСКОХОЗЯЙСТВЕННЫЕ МАШИНЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('39', 'ГОРЯЧИЕ ПРОСТОИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1025', 'Вагон-контейнер');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1018', 'Полиция и прокуратура');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('74', 'ПРОДУКТЫ ПЕРЕМОЛА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1006', 'Приволжский ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('66', 'МАШИНЫ, СТАНКИ, ДВИГАТЕЛИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('42', 'КОЛ-ВО РАССМ. ОБРАЩЕНИЙ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('47', 'УСТ. ЗАВ. ТЕХН. ТРЕБОВАНИЙ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1035', 'Тема обращения ЕИСЦ: Заключение договоров');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('45', 'ОТКАЗ В ДОПУСКЕ К УЧАСТИЮ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1039', 'Тема обращения ЕИСЦ: Благодарности');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1004', 'Сибирский ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('40', 'СТОРОННИЕ НА ZALOBAZAKUPKI@');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1021', 'Социально-культурная сфера');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1000', 'Центральный ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('78', 'РЫБА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('58', 'ГРАНУЛИРОВАННЫЕ ШЛАКИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('48', 'НА АДРЕС ZALOBAZAKUPKI@');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1007', 'Гаражи для личных нужд');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('61', 'ЖМЫХИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1033', 'Тема обращения ЕИСЦ: Дополнительные услуги');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1036', 'Тема обращения ЕИСЦ: Предоставление вагонов под перевозку');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('30', 'ПРОЧИЕ ЗА ИСКЛЮЧЕНИЕМ ТРАНСПОР');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1037', 'Тема обращения ЕИСЦ: Финансовые расчеты');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1013', 'Машиноместа');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('76', 'ПРОЧИЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('67', 'МЕТАЛЛИЧЕСКИЕ КОНСТРУКЦИИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('26', 'ЦИСТЕРНЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('70', 'ОГНЕУПОРЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1003', 'Дальневосточный ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1023', 'Складское назначение');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('35', 'ГРУЗОВОЕ ДВИЖЕНИЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('80', 'САХАРНАЯ СВЕКЛА И СЕМЕНА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('84', 'ТОРФ И ТОРФЯНАЯ ПРОДУКЦИЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('25', 'ПОЛУВАГОНЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1017', 'Офисное назначение');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1026', 'Тема обращения ЕИСЦ:  Перевозка сборной/мелкой партии/разовая');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('79', 'САХАР');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1024', 'Торговое назначение');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1008', 'Жилого назначения');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('36', 'ПАССАЖИРСКОЕ ДВИЖЕНИЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('75', 'ПРОМ.ТОВАРЫ НАР.ПОТРЕБЛЕНИЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('29', 'РЕФРИЖЕРАТОРЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('77', 'РЕЛЬСЫ И СКРЕПЛЕНИЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('0F', 'ХИМИЧЕСКИЕ И МИН. УДОБРЕНИЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('9', 'ЧЕРНЫЕ МЕТАЛЛЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1030', 'Тема обращения ЕИСЦ: Справочный запрос');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('38', 'МАНЕВРОВАЯ РАБОТА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('69', 'МЯСО И МАСЛО ЖИВОТНОЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1010', 'Незастроенный земельный участок');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('41', 'СТОРОННИЕ ИНЫМ СПОСОБОМ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('64', 'КОЛЕСНЫЕ ПАРЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('32', 'ЗЕРНОВОЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('72', 'ОСТАЛЬНЫЕ ПРОД. ТОВАРЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('8', 'РУДА ЦВ. И СЕРНОЕ СЫРЬЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('88', 'ШПАЛЫ СЫРЫЕ И ПРОПИТАННЫЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('56', 'ВСЕГО ПО НЕРАБОЧЕМУ ПАРКУ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('51', 'НЕОБОСНОВАННЫЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1019', 'Производственная деятельность');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1016', 'Общественное питание');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1015', 'Сфера образования');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1014', 'Незавершенное строительство');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('24_dfe', 'ГРУЗЫ В КОНТЕЙНЕРАХ ДФЭ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('28', 'КРЫТЫЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('49', 'ОБОСНОВАННЫЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('2', 'КАМЕННЫЙ УГОЛЬ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('44', 'КОЛ-ВО ОТМЕНЕННЫХ ПРОЦЕДУР');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('50', 'ЧАСТИЧНО ОБОСНОВАННЫЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('33', 'ФИТИНГОВЫЕ ПЛАТФОРМЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('3', 'КОКС');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('62', 'ИМПОРТНЫЕ ГРУЗЫ С УЧЕТОМ ТРАНЗИТА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('31', 'ЦЕМЕНТОВОЗЫ И МИНЕРАЛОВОЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1020', 'Путепроводы автодорожные');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('4', 'НЕФТЬ И НЕФТЕПРОДУКТЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('24', 'ГРУЗЫ В КОНТЕЙНЕРАХ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('86', 'ХЛОПОК');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('other_det', 'Прочее детализированное');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('68', 'МЕТИЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('82', 'СЛАНЦЫ ГОРЮЧИЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('52', 'ОСТАВЛЕНО БЕЗ РАССМОТРЕНИЯ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('53', 'АВТОМОБИЛИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('63', 'КАРТОФЕЛЬ, ОВОЩИ И ФРУКТЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('59', 'ДРОВА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1040', 'Тема обращения ЕИСЦ: Мобильные приложения');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('73', 'ПЕРЕВАЛКА ГРУЗ. С ВОД.ТР-ТА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('54', 'БАЛЛАСТ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1034', 'Тема обращения ЕИСЦ: Оформление док.связанных грузов');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('34', 'НЕФТЕБЕНЗИНОВЫЕ ЦИСТЕРНЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1029', 'Тема обращения ЕИСЦ: Перевозка грузов и  порожних вагонов');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1032', 'Тема обращения ЕИСЦ: ЭТП ГП');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('37', 'ХОЗЯЙСТВЕННОЕ ДВИЖЕНИЕ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1011', 'Иное');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('57', 'ВСЕГО ПОГРУЖЕНО И НАЛИТО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('10', 'ХИМИКАТЫ И СОДА');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('27', 'ПЛАТФОРМЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('43', 'КОЛ-ВО ЖАЛОБ НА РАССМОТРЕНИИ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('0B', 'ЛОМ ЧЕРНЫХ МЕТАЛЛОВ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1005', 'Уральский ФО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('-1038', 'Тема обращения ЕИСЦ: Перевозка грузов  в реф.секциях ИВ-термосах');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('1D', 'ЗЕРНО');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('11', 'СТРОИТЕЛЬНЫЕ ГРУЗЫ');

INSERT INTO stg_skim_cargo_type_t (cargo_type, name) 
VALUES ('|~|NULL|~|', 'Не задано');

-- Import Data into table stg_skim_cargo_type_t from file D:\amenyashev\Desktop\1\skim_cargo_type_t.csv . Task successful and sent to worksheet.


CREATE TABLE stg_skim_data_type_t ( id VARCHAR(26),
name VARCHAR(26));



INSERT INTO stg_skim_data_type_t (id, name) 
VALUES ('3', 'Фактические');

INSERT INTO stg_skim_data_type_t (id, name) 
VALUES ('4', 'Статистические');

INSERT INTO stg_skim_data_type_t (id, name) 
VALUES ('1', 'Оперативные');

INSERT INTO stg_skim_data_type_t (id, name) 
VALUES ('2', 'Утвержденные');

INSERT INTO stg_skim_data_type_t (id, name) 
VALUES ('|~|NULL|~|', 'Не задано');

-- Import Data into table stg_skim_data_type_t from file D:\amenyashev\Desktop\1\skim_data_type_t.csv . Task successful and sent to worksheet.

CREATE TABLE stg_skim_date_type ( id i,
name VARCHAR(26));



INSERT INTO stg_skim_date_type (id, name) 
VALUES (5, 'Квартал');

INSERT INTO stg_skim_date_type (id, name) 
VALUES (3, 'Неделя');

INSERT INTO stg_skim_date_type (id, name) 
VALUES (1, 'Год');

INSERT INTO stg_skim_date_type (id, name) 
VALUES (2, 'Месяц');

INSERT INTO stg_skim_date_type (id, name) 
VALUES (4, 'Сутки');


CREATE TABLE stg_skim_metric_type_t ( id integer,
name VARCHAR(128));



INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (21, 'Отклонение факта к факту прошлого года, %');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (22, 'Отклонение факта к плану, %');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (23, 'Отклонение факта к директивному плану, %');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (25, 'Абсолютное отклонение факта оперативного к плану');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (24, 'Абсолютное отклонение факта оперативного к директивному плану');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (26, 'Выполнение  плана');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (27, 'Выполнение директивного плана');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (28, 'Абсолютное отклонение факта к факту прошлого года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (29, 'Расчетный поправочный коэффициент');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (30, 'Влияние');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (112, 'Абсолютное отклонение факта оперативного по дороге от факта оперативного по сети');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (104, 'План (целевое значение)');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (17, 'Факт');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (109, 'Отклонение доли к доле прошлого года утв(для метрики 55)');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (103, 'Ожидаемый факт к ДПР базовый абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (19, 'Отклонение факта расследованного');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (39, 'Доля факта рассл к факту рассл идентичного периода прошлого года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (6, 'Абсолютное отклонение к базовому плану');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (50, 'ДПР ФП');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (100, 'факт утв к ДПР оптимистичный абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (35, 'ДПР ФП к 2017 отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (98, 'Ожидаемый факт к ДПР базовый отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (51, 'УФП к 2017 отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (1, 'Факт расследованный');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (55, 'ДПР ФП к 2017 абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (72, 'Факт к ДПР отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (7, 'Отклонение');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (48, 'Доля факта к плану идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (41, 'факт  к факту 2017 абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (8, 'Абсолютное отклонение');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (5, 'Базовый план');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (71, 'Доля факта к факту идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (94, 'факт ожидаемый к ДПР оптимистичный абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (37, 'Доля плана к факту рассл года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (3, 'Экспертная оценка');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (45, 'ДПР базовый к 2017 абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (53, 'ДПР баз к ДПР баз прошлого года, абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (97, 'факт утв к ДПР базовый абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (10, 'Факт разобранный');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (76, 'Доля плана к факту рассл идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (79, 'Доля факта рассл к плану года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (12, 'План');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (70, 'Доля факта к факту идентичного периода прошлого года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (34, 'ДПР оптимистичный');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (96, 'Доля факта разобр к факту разобр идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (40, 'ДПР базовый к 2017 отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (32, 'ДПР баз след год к ДПР ФП отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (106, 'Доля факта ожид к факту ожид идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (9, 'Пороговое значение');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (18, 'План с корректировкой');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (77, 'УФП к 2017 абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (107, 'Доля факта ожид к плану идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (95, 'факт утв к ДПР базовый отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (75, 'ДПР баз след год к ДПР ФП абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (2, 'Директивный план');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (49, 'Доля факта рассл к факту рассл идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (46, 'Доля факта  рассл к плану идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (108, 'Доля факта ожид к плану года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (4, 'Отклонение к директивному плану');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (31, 'ДПР базовый');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (110, 'Отклонение доли к доле прошлого года оперативно(для метрики 47)');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (52, 'УФП к факту прошлого года отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (73, 'Факт к ДПР абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (36, 'ДПР ФП к факту прошлого года абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (15, 'Отклонение факта ожидаемого');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (47, 'факт  к факту 2017 отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (74, 'УФП');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (14, 'Ожидаемый факт');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (43, 'ДПР баз к ДПР баз прошлого года, отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (105, 'Доля факта ожид к факту ожид идентичного периода прошлого года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (56, 'Доля плана к факту идентичного периода');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (99, 'факт ожидаемый к ДПР оптимистичный отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (44, 'УФП к факту прошлого года абс');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (101, 'факт утв к ДПР оптимистичный отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (38, 'ДПР ФП к факту прошлого года отн');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (33, 'Доля плана к факту года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (78, 'Доля плана к плану идентичного периода прошлого года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (20, 'Прогноз');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (13, 'Пороговое расследованное значение');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (16, 'Отклонение к базовому плану');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (42, 'Доля факта к плану года');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (11, 'Отклонение факта разобранного');

INSERT INTO stg_skim_metric_type_t (id, name) 
VALUES (54, 'Доля плана к плану года');

CREATE TABLE stg_skim_org_t ( bk integer,
id integer,
root_id integer,
child_id integer,
vname VARCHAR(256),
name VARCHAR(128));



INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028018, 13022, 10, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦДТВ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3298, -1000088, NULL, NULL, 'ШУШАРЫ-ЧЕТ', 'ШУШАРЫ-ЧЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028072, 13030, 61, 'По вине: ВРК-3 на территории дороги: Приволжская', 'ВРК-3 на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027866, 2686, 76, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Свердловская', 'ЦП на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52480, -1000141, NULL, NULL, 'ТЕМРЮК (ПЕРЕВ.)', 'ТЕМРЮК-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028271, -1028193, 1, 'Новгородская область на территории Октябрьской дороги', 'Новгородская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027932, 7398, 83, 'По вине: АО "ФПК" на территории дороги: Западно-Сибирская', 'АО "ФПК" на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027924, 7398, 24, 'По вине: АО "ФПК" на территории дороги: Горьковская', 'АО "ФПК" на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028046, 13028, 92, 'По вине: ВРК-1 на территории дороги: Восточно-Сибирская', 'ВРК-1 на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027660, -1000001, 17, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027707, NULL, NULL, 'Грузоотправитель', 'Грузоотправитель');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027592, 2634, 80, 'Южно-Уральская,ЦУКС', 'Южно-Уральская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027931, 7398, 80, 'По вине: АО "ФПК" на территории дороги: Южно-Уральская', 'АО "ФПК" на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2597, NULL, NULL, 'ЦФ', 'ЦФ OAO "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027636, 13419, 96, 'Дальневосточная,ЦДИ ОАО "РЖД"', 'Дальневосточная,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028181, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦРИ, ЦУА, ЦУО', 'ЦРИ, ЦУА, ЦУО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028326, -1028205, 58, 'Рязанская область на территории Юго-восточной дороги', 'Рязанская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2612, NULL, NULL, 'ДЕПАРТАМЕНТ УПРАВЛЕНИЯ ПЕРСОНАЛОМ ОАО "РЖД"', 'ЦКАДР ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027647, -1000001, 96, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027520, 2634, 17, 'Московская,ЦУКС', 'Московская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2499, NULL, NULL, 'ЦРЖ', 'ЦРЖ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027557, 13861, 51, 'Северо-Кавказская,ДКРЭ', 'Северо-Кавказская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027689, NULL, NULL, 'Организации - потребители услуг пригородной пассажирской инфраструктуры', 'Организации - потребители услуг пригородной пассажирской инфраструктуры');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028237, NULL, NULL, 'Пензенская область', 'Пензенская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 17, NULL, NULL, 'Московская', 'Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 58, NULL, NULL, 'Юго-Восточная', 'Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2433, NULL, NULL, 'ЦСС', 'ЦСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027967, 8809, 94, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ДОСС на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028141, 14287, 88, 'По вине: ООО СТМ-Сервис на территории дороги: Красноярская', 'ООО СТМ-Сервис на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000036, -2003, 17, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028156, 15508, 83, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Западно-Сибирская', 'ЦДИМ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028261, NULL, NULL, 'Республика Саха (Якутия)', 'Республика Саха (Якутия)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027692, NULL, NULL, 'Сервисные организации ДОСС', 'Сервисные организации ДОСС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (88857, -1000163, NULL, NULL, 'ЗАРЕЧНАЯ', 'ЗАРЕЧНАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028133, 14287, 28, 'По вине: ООО СТМ-Сервис на территории дороги: Северная', 'ООО СТМ-Сервис на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028344, -1028214, 76, 'Пермский край на территории Свердловской дороги', 'Пермский край на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027708, NULL, NULL, 'Грузополучатель', 'Грузополучатель');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 8809, NULL, NULL, 'ДОСС', 'ДОСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027664, 2507, 41, 'По сети по вине ОАО "РЖД"', 'По сети по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027653, -1000001, 76, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2813, NULL, NULL, 'Инфраструктурный комплекс (Путевой комплекс)', 'Инфраструктурный комплекс (Путевой комплекс)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027797, 2643, 28, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Северная', 'ЦД на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027677, NULL, NULL, 'Ремонтные организации (ССПС)', 'Ремонтные организации (ССПС)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (-537, -1000061, NULL, NULL, 'ВРЕМЕННЫЙ РЗД ЛУЖСКАЯ', 'ВР. РЗД ЛУЖСКАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -129027, NULL, NULL, 'ПКТБ Л', 'ПКТБ Л');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028258, NULL, NULL, 'Республика Бурятия', 'Республика Бурятия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028123, 13922, 80, 'По вине: ООО "Локотех-Сервис" на территории дороги: Южно-Уральская', 'ООО "Локотех-Сервис" на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027633, 6427, 96, 'Дальневосточная,ДКСС ОАО "РЖД"', 'Дальневосточная,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027552, 2634, 51, 'Северо-Кавказская,ЦУКС', 'Северо-Кавказская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98020, -1027643, NULL, NULL, 'ВЛАДИВОСТОК (ЭКСП.)', 'ВЛАДИВОСТ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028137, 14287, 63, 'По вине: ООО СТМ-Сервис на территории дороги: Куйбышевская', 'ООО СТМ-Сервис на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (-266, -1000063, NULL, NULL, 'ВЫБОРГ-ПОРТ', 'ВЫБОРГ-ПОРТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028117, 13922, 28, 'По вине: ООО "Локотех-Сервис" на территории дороги: Северная', 'ООО "Локотех-Сервис" на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028007, 13015, 58, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦМ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027721, -1027666, 63, 'По вине: Сервисные локомотивные депо на территории дороги: Куйбышевская', 'СЛД (Локотех и СТМ) на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027758, 2391, 92, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦЛ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027812, 2645, 24, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Горьковская', 'ЦВ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028293, -1028212, 24, 'Удмуртская Республика на территории Горьковской дороги', 'Удмуртская Республика на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027766, 2433, 51, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦСС на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1480, -1000065, NULL, NULL, 'БЕЛОЕ МОРЕ', 'БЕЛОЕ МОРЕ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028094, 13316, 92, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦДМВ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028251, NULL, NULL, 'Алтайский край', 'Алтайский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2350, -1000082, NULL, NULL, 'ВЫСОЦК (ПЕРЕВ.)', 'ВЫСОЦК-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027834, 2646, 76, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ГВЦ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027817, 2645, 63, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦВ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028029, 13022, 88, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦДТВ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027525, 13861, 17, 'Московская,ДКРЭ', 'Московская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027874, 7280, 10, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Калининградская', 'ДМО на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028099, 13317, 17, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Московская', 'ЦДПО на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000012, 2507, 80, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028049, 13029, 1, 'По вине: ВРК-2 на территории дороги: Октябрьская', 'ВРК-2 на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028297, -1028215, 24, 'Кировская область на территории Горьковской дороги', 'Кировская область на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027895, 7383, 58, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦФТО на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7385, NULL, NULL, 'УПРАВЛЕНИЕ ЭКСПЕРТИЗЫ ПРОЕКТОВ И СМЕТ ОАО "РЖД"', 'ЦУЭП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027759, 2391, 94, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦЛ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027774, 2433, 92, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦСС на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000019, -2507, 10, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027828, 2646, 24, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ГВЦ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028077, 13030, 88, 'По вине: ВРК-3 на территории дороги: Красноярская', 'ВРК-3 на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027756, 2391, 83, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦЛ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027889, 7383, 1, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦФТО на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7640, -1000101, NULL, NULL, 'ЛУЖСКАЯ (ЭКСП.)', 'ЛУЖСКАЯ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027790, 2597, 92, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦФ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13922, NULL, NULL, 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ЛОКОТЕХ-СЕРВИС"', 'ООО "ЛОКОТЕХ-СЕРВИС"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028366, -1028252, 88, 'Кемеровская область на территории Красноярской дороги', 'Кемеровская область на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3560, -1000093, NULL, NULL, 'АВТОВО (ЭКСП.)', 'АВТОВО-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027512, 2634, 10, 'Калининградская,ЦУКС', 'Калининградская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027880, 7280, 61, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Приволжская', 'ДМО на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027961, 8809, 63, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ДОСС на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028346, -1028217, 76, 'Свердловская область на территории Свердловской дороги', 'Свердловская область на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52310, -1000138, NULL, NULL, 'ВЫШЕСТЕБЛИЕВСКАЯ (НАЛИВ)(ЭКСП.)', 'ВЫШСТБЛ-НАЛ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028169, 15773, 63, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Куйбышевская', 'ЦДМ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53310, -1000155, NULL, NULL, 'ТУАПСЕ-СОРТИРОВОЧНАЯ (ЭКСП.)', 'ТУАПСЕ-СОРТ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -124361, NULL, NULL, 'НПЦ ООС', 'НПЦ ООС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028315, -1028229, 51, 'Республика Северная Осетия-Алания на территории Северо-кавказской дороги', 'Республика Северная Осетия-Алания на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028054, 13029, 51, 'По вине: ВРК-2 на территории дороги: Северо-Кавказская', 'ВРК-2 на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027663, -1000001, 41, 'По сети вне ответственности ОАО "РЖД"', 'По сети вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027736, -1000000, 61, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Приволжская', 'Холдинг на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13316, NULL, NULL, 'ЦДМВ', 'ЦДМВ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027802, 2643, 76, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦД на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000014, 2507, 88, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028263, NULL, NULL, 'Хабаровский край', 'Хабаровский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51141, -1000114, NULL, NULL, 'ТАГАНРОГ-ПАССАЖИРСКИЙ', 'ТАГАНРОГ-ПАС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028038, 13028, 51, 'По вине: ВРК-1 на территории дороги: Северо-Кавказская', 'ВРК-1 на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027528, 2634, 24, 'Горьковская,ЦУКС', 'Горьковская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027811, 2645, 17, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Московская', 'ЦВ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 92, NULL, NULL, 'Восточно-Сибирская', 'Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027851, 2678, 80, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦШ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027813, 2645, 28, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Северная', 'ЦВ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028313, -1028227, 51, 'Республика Калмыкия на территории Северо-кавказской дороги', 'Республика Калмыкия на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (86930, -1000162, NULL, NULL, 'АЗОВСКАЯ', 'АЗОВСКАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027921, 7398, 1, 'По вине: АО "ФПК" на территории дороги: Октябрьская', 'АО "ФПК" на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027815, 2645, 58, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦВ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14287, NULL, NULL, 'ООО СТМ-сервис', 'ООО СТМ-сервис');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028143, 14287, 94, 'По вине: ООО СТМ-Сервис на территории дороги: Забайкальская', 'ООО СТМ-Сервис на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027522, 7230, 17, 'Московская,ДКРС', 'Московская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027519, -3619, 17, 'Московская,ТЭ', 'Московская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027939, 7728, 17, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Московская', 'ДЖВ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028078, 13030, 92, 'По вине: ВРК-3 на территории дороги: Восточно-Сибирская', 'ВРК-3 на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027974, 12077, 51, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦТР на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52095, -1000132, NULL, NULL, 'НОВОРОССИЙСК САИ-ГАЙД', 'НОВОРОССИЙСК САИ-ГАЙД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027826, 2646, 10, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ГВЦ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028254, NULL, NULL, 'Томская область', 'Томская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028233, NULL, NULL, 'Ростовская область', 'Ростовская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027698, NULL, NULL, 'Грузополучатель', 'Грузополучатель');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7660, -1000103, NULL, NULL, 'ЛУЖСКАЯ ПАРОМ', 'ЛУЖСКАЯ-ПАР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027558, -9998, 58, 'Юго-Восточная,Прочие ОАО "РЖД"', 'Юго-Восточная,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027696, NULL, NULL, 'Железнодорожные администрации стран СНГ и Балтии (пасс)', 'Железнодорожные администрации стран СНГ и Балтии (пасс)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028128, 13922, 96, 'По вине: ООО "Локотех-Сервис" на территории дороги: Дальневосточная', 'ООО "Локотех-Сервис" на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028103, 13317, 58, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦДПО на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028376, -1028260, 96, 'Амурская область на территории Дальневосточной дороги', 'Амурская область на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028010, 13015, 76, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦМ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027503, -3619, 1, 'Октябрьская,ТЭ', 'Октябрьская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028035, 13028, 17, 'По вине: ВРК-1 на территории дороги: Московская', 'ВРК-1 на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13317, NULL, NULL, 'ЦДПО', 'ЦДПО ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 91, NULL, NULL, 'ЖДЯ', 'Железные дороги Якутии');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028079, 13030, 94, 'По вине: ВРК-3 на территории дороги: Забайкальская', 'ВРК-3 на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7728, NULL, NULL, 'ДЖВ', 'ДЖВ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027598, -9998, 83, 'Западно-Сибирская,Прочие ОАО "РЖД"', 'Западно-Сибирская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027788, 2597, 83, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦФ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7387, NULL, NULL, 'ЖЕЛДОРУЧЕТ', 'ЖЕЛДОРУЧЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027673, NULL, NULL, 'Итого по сторонним организациям (груз)', 'Итого по сторонним организациям (груз)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028179, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦЮ, ЦСАР, ЦНТД', 'ЦЮ, ЦСАР, ЦНТД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028172, 15773, 83, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Западно-Сибирская', 'ЦДМ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028185, NULL, NULL, 'Территориальные Центры фирменного транспортного обслуживания', 'ТЦФТО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028301, -1028218, 28, 'Республика Коми на территории Северной дороги', 'Республика Коми на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000037, -2003, 24, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028377, -1028264, 96, 'Сахалинская область на территории Дальневосточной дороги', 'Сахалинская область на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028239, NULL, NULL, 'Тамбовская область', 'Тамбовская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027841, 2678, 1, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Октябрьская', 'ЦШ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028305, -1028220, 28, 'Ивановская область на территории Северной дороги', 'Ивановская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027748, 2391, 24, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦЛ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028227, NULL, NULL, 'Республика Калмыкия', 'Республика Калмыкия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98090, -1027645, NULL, NULL, 'МЫС-ЧУРКИН (ЭКСП.)', 'МЫС ЧУР-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027965, 8809, 88, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Красноярская', 'ДОСС на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -129300, NULL, NULL, 'Центр по организации противодействия коррупции', 'ЦОПК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1840, -1000069, NULL, NULL, 'МУРМАНСК', 'МУРМАНСК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98002, -1000170, NULL, NULL, 'ВЛАДИВОСТОК-ЧЕТ', 'ВЛАДИВОСТОК-ЧЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 8769, NULL, NULL, 'ДЕПАРТАМЕНТ ПО ОРГАНИЗАЦИИ, ОПЛАТЕ И МОТИВАЦИИ ТРУДА ОАО "РЖД"', 'ЦЗТ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027693, NULL, NULL, 'Вагоноремонтные заводы (пасс)', 'Вагоноремонтные заводы (пасс)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028177, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦОПР (1)', 'ЦОПР (1)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027773, 2433, 88, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦСС на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2014, NULL, NULL, 'Строительно-монтажные организации', 'Строительно-монтажные организации');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028085, 13316, 28, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦДМВ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7280, NULL, NULL, 'ДМО', 'ДМО ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52092, -1000131, NULL, NULL, 'НОВОРОССИЙСК-ВХОД', 'НОВОРОССИЙСК-ВХОД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027881, 7280, 63, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ДМО на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027703, NULL, NULL, 'Другие сторонние организации, сервисные', 'Другие сторонние организации, сервисные');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028108, 13317, 83, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦДПО на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028213, NULL, NULL, 'Чувашская Республика', 'Чувашская Республика');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027996, 13006, 83, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦТ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98600, -1000190, NULL, NULL, 'НАХОДКА-ВОСТОЧНАЯ (ПЕРЕВ.)', 'НАХОДКА-В-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027769, 2433, 63, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦСС на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2811, NULL, NULL, 'Локомотивный комплекс', 'Локомотивный комплекс');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000039, -2003, 51, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2010, NULL, NULL, 'Операторы и собственники', 'Операторы и собственники');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13419, NULL, NULL, 'ЦДИ', 'ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028331, -1028235, 61, 'Волгоградская область на территории Приволжской дороги', 'Волгоградская область на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028257, NULL, NULL, 'Иркутская область', 'Иркутская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027634, 7230, 96, 'Дальневосточная,ДКРС', 'Дальневосточная,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028342, -1028244, 63, 'Челябинская область на территории Куйбышевской дороги', 'Челябинская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027784, 2597, 61, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦФ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028294, -1028213, 24, 'Чувашская Республика на территории Горьковской дороги', 'Чувашская Республика на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -127710, NULL, NULL, 'Центр внутреннего аудита "Желдораудит"', 'ЦЖДА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000001, NULL, NULL, 'Вне ответственности РЖД', 'Вне ответственности РЖД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027678, NULL, NULL, 'Машиностроительные заводы (ССПС)', 'Машиностроительные заводы (ССПС)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7230, NULL, NULL, 'ДКРС', 'ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028154, 15508, 76, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Свердловская', 'ЦДИМ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027699, NULL, NULL, 'Грузоотправитель', 'Грузоотправитель');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027886, 7280, 92, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ДМО на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027950, 7728, 92, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ДЖВ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027605, 13861, 83, 'Западно-Сибирская,ДКРЭ', 'Западно-Сибирская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2507, NULL, NULL, 'ДЗО', 'ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53336, -1000156, NULL, NULL, 'ТУАПСЕ-ПАССАЖИРСКАЯ', 'ТУАПСЕ-ПАС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028009, 13015, 63, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦМ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028333, -1028208, 63, 'Республика Башкортостан на территории Куйбышевской дороги', 'Республика Башкортостан на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027601, 6427, 83, 'Западно-Сибирская,ДКСС ОАО "РЖД"', 'Западно-Сибирская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027757, 2391, 88, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦЛ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98570, -1027641, NULL, NULL, 'МЫС АСТАФЬЕВА (ЭКСП.)', 'МЫС АСТАФ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028247, NULL, NULL, 'Ханты-Мансийский автономный округ Югра', 'Ханты-Мансийский автономный округ Югра');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000047, -2003, 92, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2507, NULL, NULL, 'ОАО РЖД', 'ОАО РЖД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027583, -3619, 76, 'Свердловская,ТЭ', 'Свердловская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028264, NULL, NULL, 'Сахалинская область', 'Сахалинская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027887, 7280, 94, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ДМО на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2012, NULL, NULL, 'Заводы инфраструктурные', 'Заводы инфраструктурные');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98610, -1000191, NULL, NULL, 'НАХОДКА-ВОСТОЧНАЯ (ЭКСП.)', 'НАХОДКА-ВОСТ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53341, -1000159, NULL, NULL, 'ТУАПСЕ-СОРТ-ПАРК ПРИБЫТИЯ НЕЧ', 'ТУАПСЕ-С-П ПРИБЫТИЯ НЕЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028316, -1028230, 51, 'Чеченская республика на территории Северо-кавказской дороги', 'Чеченская республика на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027872, 2686, 96, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦП на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3291, -1000084, NULL, NULL, 'ШУШАРЫ-ЦАР.С', 'ШУШАРЫ-ЦАР.С');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027574, -9998, 63, 'Куйбышевская,Прочие ОАО "РЖД"', 'Куйбышевская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028020, 13022, 24, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦДТВ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027741, -1000000, 88, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Красноярская', 'Холдинг на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (-345, -1000062, NULL, NULL, 'ВАНИНО-ЮГ', 'ВАНИНО-ЮГ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027513, 6427, 10, 'Калининградская,ДКСС ОАО "РЖД"', 'Калининградская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027829, 2646, 28, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Северная', 'ГВЦ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2622, NULL, NULL, 'ЦТЕХ', 'ЦТЕХ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 51, NULL, NULL, 'Северо-Кавказская', 'Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027508, 13419, 1, 'Октябрьская,ЦДИ ОАО "РЖД"', 'Октябрьская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2554, NULL, NULL, 'ДЕПАРТАМЕНТ КОРПОРАТИВНОГО ИМУЩЕСТВА', 'ЦРИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13030, NULL, NULL, 'ВРК-3', 'ВРК-3');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027862, 2686, 51, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦП на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51690, -1000123, NULL, NULL, 'СТАРОМИНСКАЯ-ЕЙСКАЯ (ПЕРЕВ.)', 'СТАРОМИНСКАЯ-ЕЙСКАЯ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027979, 12077, 80, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦТР на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000030, -2507, 88, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027935, 7398, 94, 'По вине: АО "ФПК" на территории дороги: Забайкальская', 'АО "ФПК" на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -128734, NULL, NULL, 'ЦУЗПД', 'ЦУЗПД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028296, -1028200, 24, 'Владимирская область на территории Горьковской дороги', 'Владимирская область на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027796, 2643, 24, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦД на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027930, 7398, 76, 'По вине: АО "ФПК" на территории дороги: Свердловская', 'АО "ФПК" на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027649, -1000001, 92, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000024, -2507, 58, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028175, 15773, 94, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Забайкальская', 'ЦДМ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027997, 13006, 88, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦТ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000034, -2003, 1, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028291, -1028210, 24, 'Республика Мордовия на территории Горьковской дороги', 'Республика Мордовия на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028176, 15773, 96, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Дальневосточная', 'ЦДМ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000048, -2003, 94, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2001, NULL, NULL, 'ЦДИМ', 'ЦДИМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028044, 13028, 83, 'По вине: ВРК-1 на территории дороги: Западно-Сибирская', 'ВРК-1 на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 96, NULL, NULL, 'Дальневосточная', 'Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027789, 2597, 88, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦФ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027656, -1000001, 58, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027738, -1000000, 76, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Свердловская', 'Холдинг на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027620, 13419, 92, 'Восточно-Сибирская,ЦДИ ОАО "РЖД"', 'Восточно-Сибирская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027714, -1027666, 10, 'По вине: Сервисные локомотивные депо на территории дороги: Калининградская', 'СЛД (Локотех и СТМ) на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028359, -1028253, 83, 'Новосибирская область на территории Западно-сибирской дороги', 'Новосибирская область на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -124106, NULL, NULL, 'ЦНТИБ', 'ЦНТИБ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028075, 13030, 80, 'По вине: ВРК-3 на территории дороги: Южно-Уральская', 'ВРК-3 на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027623, -3619, 94, 'Забайкальская,ТЭ', 'Забайкальская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52619, -1000143, NULL, NULL, 'БЛОКПОСТ КАВКАЗСКИЙ', 'БП КАВКАЗСКИЙ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028153, 15508, 63, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Куйбышевская', 'ЦДИМ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028207, NULL, NULL, 'Тульская область', 'Тульская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027562, 7230, 58, 'Юго-Восточная,ДКРС', 'Юго-Восточная,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027585, 6427, 76, 'Свердловская,ДКСС ОАО "РЖД"', 'Свердловская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028146, 15508, 10, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Калининградская', 'ЦДИМ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027586, 7230, 76, 'Свердловская,ДКРС', 'Свердловская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3540, -1000091, NULL, NULL, 'АВТОВО (ПЕРЕВ.)', 'АВТОВО-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027713, -1027666, 1, 'По вине: Сервисные локомотивные депо на территории дороги: Октябрьская', 'СЛД (Локотех и СТМ) на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027806, 2643, 92, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦД на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14453, NULL, NULL, 'ДЕПАРТАМЕНТ УПРАВЛЕНИЯ БИЗНЕС-БЛОКОМ "ЖЕЛЕЗНОДОРОЖНЫЕ ПЕРЕВОЗКИ И ИНФРАСТРУКТУРА" ОАО "РЖД"', 'ЦЖД ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000002, 2507, 1, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13022, NULL, NULL, 'ЦДТВ', 'ЦДТВ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2592, NULL, NULL, 'ЖЕЛДОРРАСЧЕТ', 'ЖЕЛДОРРАСЧЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7074, NULL, NULL, 'ЦИНВ', 'ЦИНВ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98590, -1000189, NULL, NULL, 'НАХОДКА-ВОСТОЧНАЯ', 'НАХОДКА-ВОСТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027873, 7280, 1, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ДМО на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027734, -1000000, 51, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Северо-Кавказская', 'Холдинг на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98470, -1000178, NULL, NULL, 'НАХОДКА (ЭКСП.)', 'НАХОДКА-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028328, -1028239, 58, 'Тамбовская область на территории Юго-восточной дороги', 'Тамбовская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 80, NULL, NULL, 'Южно-Уральская', 'Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028052, 13029, 24, 'По вине: ВРК-2 на территории дороги: Горьковская', 'ВРК-2 на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -9998, NULL, NULL, 'Прочие ОАО "РЖД"', 'Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000000, NULL, NULL, 'Холдинг', 'Холдинг');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028187, NULL, NULL, 'ООО "ИНТЕЛЛЕКС"', 'ИНТЕЛЛЕКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13861, NULL, NULL, 'ДКРЭ', 'ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027993, 13006, 63, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦТ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028131, 14287, 17, 'По вине: ООО СТМ-Сервис на территории дороги: Московская', 'ООО СТМ-Сервис на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027744, -1000000, 96, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Дальневосточная', 'Холдинг на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027852, 2678, 83, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦШ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027969, 12077, 1, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦТР на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13015, NULL, NULL, 'ЦМ', 'ЦМ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027915, 7390, 80, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ТЭ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028002, 13015, 10, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦМ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028253, NULL, NULL, 'Новосибирская область', 'Новосибирская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027877, 7280, 28, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Северная', 'ДМО на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028024, 13022, 61, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦДТВ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027746, 2391, 10, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦЛ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2678, NULL, NULL, 'ЦШ', 'ЦШ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027992, 13006, 61, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦТ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98450, -1000176, NULL, NULL, 'НАХОДКА', 'НАХОДКА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027662, -1000001, 1, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027910, 7390, 51, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ТЭ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028017, 13022, 1, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦДТВ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -135898, NULL, NULL, 'Центр моделирования бизнес-процессов', 'ЦМБП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3550, -1000092, NULL, NULL, 'АВТОВО', 'АВТОВО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028259, NULL, NULL, 'Забайкальский край', 'Забайкальский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000015, 2507, 92, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027861, 2686, 28, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Северная', 'ЦП на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028135, 14287, 58, 'По вине: ООО СТМ-Сервис на территории дороги: Юго-Восточная', 'ООО СТМ-Сервис на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7631, -1000098, NULL, NULL, 'ЛУЖСКАЯ-ЮЖНАЯ', 'ЛУЖСКАЯ-ЮЖН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 88, NULL, NULL, 'Красноярская', 'Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027607, -3619, 88, 'Красноярская,ТЭ', 'Красноярская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028013, 13015, 88, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦМ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027994, 13006, 76, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦТ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028211, NULL, NULL, 'Республика Татарстан', 'Республика Татарстан');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027947, 7728, 80, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ДЖВ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027680, NULL, NULL, 'Сервисные организации ЦП, ЦДРП', 'Сервисные организации ЦП, ЦДРП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028240, NULL, NULL, 'Астраханская область', 'Астраханская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000013, 2507, 83, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027767, 2433, 58, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦСС на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028100, 13317, 24, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦДПО на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028011, 13015, 80, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦМ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000017, 2507, 96, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027936, 7398, 96, 'По вине: АО "ФПК" на территории дороги: Дальневосточная', 'АО "ФПК" на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52110, -1000134, NULL, NULL, 'ГРУШЕВАЯ (ЭКСП.)', 'ГРУШЕВАЯ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027771, 2433, 80, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦСС на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51490, -1000118, NULL, NULL, 'СОСЫКА-ЕЙСКАЯ', 'СОСЫКА-ЕЙСК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027999, 13006, 94, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦТ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028088, 13316, 61, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦДМВ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027608, 2634, 88, 'Красноярская,ЦУКС', 'Красноярская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028145, 15508, 1, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Октябрьская', 'ЦДИМ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98460, -1000177, NULL, NULL, 'НАХОДКА (ПЕРЕВ.)', 'НАХОДКА-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027752, 2391, 61, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦЛ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2010, -1000074, NULL, NULL, 'ВЫБОРГ (ЭКСП.)', 'ВЫБОРГ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51870, -1000125, NULL, NULL, 'АЗОВ (ПЕРЕВ.)', 'АЗОВ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -3619, NULL, NULL, 'ТЭ', 'ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98540, -1000185, NULL, NULL, 'КРАБОВАЯ', 'КРАБОВАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027676, NULL, NULL, 'Заводы изготовители продукции ЖАТ', 'Заводы изготовители продукции ЖАТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027720, -1027666, 61, 'По вине: Сервисные локомотивные депо на территории дороги: Приволжская', 'СЛД (Локотех и СТМ) на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027630, -9998, 96, 'Дальневосточная,Прочие ОАО "РЖД"', 'Дальневосточная,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028224, NULL, NULL, 'Республика Дагестан', 'Республика Дагестан');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 6719, NULL, NULL, 'ЦЛП', 'ЦЛП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000046, -2003, 88, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2586, NULL, NULL, 'АХУ', 'АХУ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (47230, -1000107, NULL, NULL, 'АЗОВСКАЯ', 'АЗОВСКАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027819, 2645, 80, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦВ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028203, NULL, NULL, 'Липецкая область', 'Липецкая область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028353, -1028217, 80, 'Свердловская область на территории Южно-уральской дороги', 'Свердловская область на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027778, 2597, 10, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦФ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027544, 2634, 41, 'СЕТЬ,ЦУКС', 'СЕТЬ,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98010, -1027642, NULL, NULL, 'ВЛАДИВОСТОК (ПЕРЕВ.)', 'ВЛАДИВОСТ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028097, 13317, 1, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦДПО на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000023, -2507, 51, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 42, NULL, NULL, 'СЕТЬ с учетом ЖДЯ', 'СЕТЬ с учетом ЖДЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -9999, NULL, NULL, 'Остальное КАСАНТ\КАСАТ', 'Остальное КАСАНТ\КАСАТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7398, NULL, NULL, 'АКЦИОНЕРНОЕ ОБЩЕСТВО "ФЕДЕРАЛЬНАЯ ПАССАЖИРСКАЯ КОМПАНИЯ"- ДО ОАО "РЖД"', 'АО "ФПК"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028056, 13029, 61, 'По вине: ВРК-2 на территории дороги: Приволжская', 'ВРК-2 на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027732, -1000000, 24, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Горьковская', 'Холдинг на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027725, -1027666, 88, 'По вине: Сервисные локомотивные депо на территории дороги: Красноярская', 'СЛД (Локотех и СТМ) на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1720, -1000067, NULL, NULL, 'КОЛА (ЭКСП.)', 'КОЛА-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027596, 13419, 80, 'Южно-Уральская,ЦДИ ОАО "РЖД"', 'Южно-Уральская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027980, 12077, 83, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦТР на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028214, NULL, NULL, 'Пермский край', 'Пермский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52820, -1000150, NULL, NULL, 'КАВКАЗ-ПАРОМ-ПОТИ-ЭКСПОРТ', 'КАВКАЗ-ПАР-ПОТИ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027675, NULL, NULL, 'Заводы изготовители оборудования (Трансэнерго)', 'Заводы изготовители оборудования (Трансэнерго)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98449, -1000175, NULL, NULL, 'НАХОДКА-БАРХ', 'НАХОДКА-БАРХ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98510, -1000182, NULL, NULL, 'РЫБНИКИ (ЭКСП.)', 'РЫБНИКИ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3293, -1000086, NULL, NULL, 'ШУШАРЫ-СРЕДН', 'ШУШАРЫ-СРЕДН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028109, 13317, 88, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦДПО на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 94, NULL, NULL, 'Забайкальская', 'Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027885, 7280, 88, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Красноярская', 'ДМО на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027927, 7398, 58, 'По вине: АО "ФПК" на территории дороги: Юго-Восточная', 'АО "ФПК" на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13834, NULL, NULL, 'ЦУУ', 'ЦУУ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027624, 2634, 94, 'Забайкальская,ЦУКС', 'Забайкальская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000028, -2507, 80, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027706, NULL, NULL, 'в т.ч. особая технологическая необходимость и внешнее воздействие', 'в т.ч. особая технологическая необходимость и внешнее воздействие');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51700, -1000124, NULL, NULL, 'ЕЙСК (ПЕРЕВ.)', 'ЕЙСК-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027668, NULL, NULL, 'Вагоностроительные заводы (груз.)', 'Вагоностроительные заводы (груз.)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028102, 13317, 51, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦДПО на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028121, 13922, 63, 'По вине: ООО "Локотех-Сервис" на территории дороги: Куйбышевская', 'ООО "Локотех-Сервис" на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028373, -1028261, 96, 'Республика Саха (Якутия) на территории Дальневосточной дороги', 'Республика Саха (Якутия) на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027717, -1027666, 28, 'По вине: Сервисные локомотивные депо на территории дороги: Северная', 'СЛД (Локотех и СТМ) на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027914, 7390, 76, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Свердловская', 'ТЭ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028167, 15773, 58, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Юго-Восточная', 'ЦДМ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028283, -1028204, 17, 'Орловская область на территории Московской дороги', 'Орловская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028244, NULL, NULL, 'Челябинская область', 'Челябинская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7390, NULL, NULL, 'ТРАНСЭНЕРГО', 'ТРАНСЭНЕРГО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027665, NULL, NULL, 'ФФ ОАО "РЖД", ДЗО  и Сторонние (все организации)', 'На инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028292, -1028211, 24, 'Республика Татарстан на территории Горьковской дороги', 'Республика Татарстан на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028118, 13922, 51, 'По вине: ООО "Локотех-Сервис" на территории дороги: Северо-Кавказская', 'ООО "Локотех-Сервис" на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028022, 13022, 51, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦДТВ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027845, 2678, 28, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Северная', 'ЦШ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027990, 13006, 51, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦТ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028336, -1028241, 63, 'Оренбургская область на территории Куйбышевской дороги', 'Оренбургская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027651, -1000001, 83, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027764, 2433, 24, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦСС на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028012, 13015, 83, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦМ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027902, 7383, 92, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦФТО на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028112, 13317, 96, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦДПО на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 8609, NULL, NULL, 'ЦДРП', 'ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028334, -1028210, 63, 'Республика Мордовия на территории Куйбышевской дороги', 'Республика Мордовия на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027959, 8809, 58, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ДОСС на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2621, NULL, NULL, 'ЦЮ', 'ЦЮ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027954, 8809, 10, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Калининградская', 'ДОСС на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028162, 15773, 10, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Калининградская', 'ЦДМ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028071, 13030, 58, 'По вине: ВРК-3 на территории дороги: Юго-Восточная', 'ВРК-3 на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027926, 7398, 51, 'По вине: АО "ФПК" на территории дороги: Северо-Кавказская', 'АО "ФПК" на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027918, 7390, 92, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ТЭ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2018, NULL, NULL, 'Транспортнологистический', 'Транспортнологистический');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028370, -1028257, 92, 'Иркутская область на территории Восточно-сибирской дороги', 'Иркутская область на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027804, 2643, 83, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦД на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027691, NULL, NULL, 'РЖДВ', 'РЖДВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (96760, -1000165, NULL, NULL, 'ВАНИНО', 'ВАНИНО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028234, NULL, NULL, 'Белгородская область', 'Белгородская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027606, -9998, 88, 'Красноярская,Прочие ОАО "РЖД"', 'Красноярская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028302, -1028219, 28, 'Архангельская область на территории Северной дороги', 'Архангельская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98770, -1000194, NULL, NULL, 'ПОСЬЕТ', 'ПОСЬЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027808, 2643, 96, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦД на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027984, 12077, 96, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦТР на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -44011857, NULL, NULL, 'Южно-Уральская дирекция пассажирских обустройств - структурное подразделение Центральной дирекции пассажирских обустройств', 'ПО Ю-УР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028268, -1028190, 1, 'Ленинградская область на территории Октябрьской дороги', 'Ленинградская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027560, 2634, 58, 'Юго-Восточная,ЦУКС', 'Юго-Восточная,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027669, NULL, NULL, 'Частные вагоноремонтные предприятия (груз.)', 'Частные вагоноремонтные предприятия (груз.)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027985, 13006, 1, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦТ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028124, 13922, 83, 'По вине: ООО "Локотех-Сервис" на территории дороги: Западно-Сибирская', 'ООО "Локотех-Сервис" на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000010, 2507, 63, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028312, -1028226, 51, 'Кабардино-Балкарская республика на территории Северо-кавказской дороги', 'Кабардино-Балкарская республика на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000027, -2507, 76, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98589, -1000188, NULL, NULL, 'НАХОДКА-ВОСТ-НЕЧ', 'НАХОДКА-ВОСТ-НЕЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027563, 8609, 58, 'Юго-Восточная,ЦДРП ОАО "РЖД"', 'Юго-Восточная,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028039, 13028, 58, 'По вине: ВРК-1 на территории дороги: Юго-Восточная', 'ВРК-1 на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7284, NULL, NULL, 'ЦЭКР', 'ЦЭКР ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14457, NULL, NULL, 'ЦКО', 'ЦКО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027509, 13861, 1, 'Октябрьская,ДКРЭ', 'Октябрьская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027831, 2646, 58, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ГВЦ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027637, 13861, 96, 'Дальневосточная,ДКРЭ', 'Дальневосточная,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -129174, NULL, NULL, 'ПКБ И', 'ПКБ И');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027830, 2646, 51, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ГВЦ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 41, NULL, NULL, 'СЕТЬ', 'СЕТЬ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027535, -3619, 28, 'Северная,ТЭ', 'Северная,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028243, NULL, NULL, 'Ульяновская область', 'Ульяновская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028057, 13029, 63, 'По вине: ВРК-2 на территории дороги: Куйбышевская', 'ВРК-2 на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7680, -1000105, NULL, NULL, 'ЛУЖСКАЯ-ПАРОМ-ЭКСПОРТ(ТР.СТР.)', 'ЛУЖСКАЯ-П-Э(ТР. СТР.)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028242, NULL, NULL, 'Самарская область', 'Самарская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027546, 7230, 41, 'СЕТЬ,ДКРС', 'СЕТЬ,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028087, 13316, 58, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦДМВ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028063, 13029, 94, 'По вине: ВРК-2 на территории дороги: Забайкальская', 'ВРК-2 на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027648, -1000001, 94, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027702, NULL, NULL, 'Другие подразделения, входящие в состав ОАО РЖД', 'Другие подразделения, входящие в состав ОАО РЖД');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027578, 7230, 63, 'Куйбышевская,ДКРС', 'Куйбышевская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027908, 7390, 24, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Горьковская', 'ТЭ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028015, 13015, 94, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦМ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028036, 13028, 24, 'По вине: ВРК-1 на территории дороги: Горьковская', 'ВРК-1 на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027972, 12077, 24, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦТР на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028341, -1028243, 63, 'Ульяновская область на территории Куйбышевской дороги', 'Ульяновская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027869, 2686, 88, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Красноярская', 'ЦП на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028196, NULL, NULL, 'г. Москва', 'г. Москва');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 15491, NULL, NULL, 'ЦИР', 'ЦИР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028371, -1028259, 94, 'Забайкальский край на территории Забайкальской дороги', 'Забайкальский край на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028288, -1028196, 17, 'г. Москва на территории Московской дороги', 'г. Москва на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028281, -1028203, 17, 'Липецкая область на территории Московской дороги', 'Липецкая область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027657, -1000001, 51, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028111, 13317, 94, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦДПО на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027727, -1027666, 94, 'По вине: Сервисные локомотивные депо на территории дороги: Забайкальская', 'СЛД (Локотех и СТМ) на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2391, NULL, NULL, 'ЦЛ', 'ЦЛ OAO "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028235, NULL, NULL, 'Волгоградская область', 'Волгоградская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028252, NULL, NULL, 'Кемеровская область', 'Кемеровская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027904, 7383, 96, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦФТО на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028363, -1028255, 88, 'Республика Хакасия на территории Красноярской дороги', 'Республика Хакасия на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028098, 13317, 10, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦДПО на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027619, 8609, 92, 'Восточно-Сибирская,ЦДРП ОАО "РЖД"', 'Восточно-Сибирская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027832, 2646, 61, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ГВЦ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98447, -1000173, NULL, NULL, 'НАХОДКА-ПАРК-Д', 'НАХОДКА-ПАРК-Д');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028095, 13316, 94, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦДМВ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028023, 13022, 58, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦДТВ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2070, -1000080, NULL, NULL, 'ВЫСОЦК (ЭКСП.)', 'ВЫСОЦК-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027504, 2634, 1, 'Октябрьская,ЦУКС', 'Октябрьская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52940, -1000153, NULL, NULL, 'КАВКАЗ-ПАРОМ-КРЫМ', 'КАВКАЗ-ПАР-КРЫМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027760, 2391, 96, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦЛ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000007, 2507, 51, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027611, 8609, 88, 'Красноярская,ЦДРП ОАО "РЖД"', 'Красноярская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028186, NULL, NULL, 'Единая служба поддержки пользователей Главного вычислительного центра ОАО "РЖД"', 'ЕСПП ГВЦ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027899, 7383, 80, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦФТО на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028157, 15508, 88, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Красноярская', 'ЦДИМ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14756, NULL, NULL, 'ЦМТП', 'ЦМТП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028026, 13022, 76, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦДТВ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028080, 13030, 96, 'По вине: ВРК-3 на территории дороги: Дальневосточная', 'ВРК-3 на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027964, 8809, 83, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ДОСС на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027602, 7230, 83, 'Западно-Сибирская,ДКРС', 'Западно-Сибирская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027550, -9998, 51, 'Северо-Кавказская,Прочие ОАО "РЖД"', 'Северо-Кавказская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027543, -3619, 41, 'СЕТЬ,ТЭ', 'СЕТЬ,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53339, -1000157, NULL, NULL, 'ТУАПСЕ-СОРТ ПУТЕВОЙ ПОСТ 1885 КМ', 'ТУАПСЕ-С ПП 1885 КМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027909, 7390, 28, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Северная', 'ТЭ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027850, 2678, 76, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Свердловская', 'ЦШ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028282, -1028191, 17, 'Московская область на территории Московской дороги', 'Московская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027610, 7230, 88, 'Красноярская,ДКРС', 'Красноярская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027793, 2643, 1, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦД на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2002, NULL, NULL, 'Энергосбытовые компании', 'Энергосбытовые компании');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027791, 2597, 94, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦФ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027857, 2686, 1, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Октябрьская', 'ЦП на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027572, 13419, 61, 'Приволжская,ЦДИ ОАО "РЖД"', 'Приволжская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027533, 13861, 24, 'Горьковская,ДКРЭ', 'Горьковская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1830, -1000068, NULL, NULL, 'МУРМАНСК (ПЕРЕВ.)', 'МУРМАНСК-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7388, NULL, NULL, 'ЦОТЭН', 'ЦОТЭН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -119416, NULL, NULL, 'Центр "Желдоррасчет"', 'ЦЖДР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028248, NULL, NULL, 'Курганская область', 'Курганская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028199, NULL, NULL, 'Брянская область', 'Брянская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027556, 13419, 51, 'Северо-Кавказская,ЦДИ ОАО "РЖД"', 'Северо-Кавказская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027777, 2597, 1, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦФ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027907, 7390, 17, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Московская', 'ТЭ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028104, 13317, 61, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦДПО на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028058, 13029, 76, 'По вине: ВРК-2 на территории дороги: Свердловская', 'ВРК-2 на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027810, 2645, 10, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Калининградская', 'ЦВ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 83, NULL, NULL, 'Западно-Сибирская', 'Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52090, -1000129, NULL, NULL, 'НОВОРОССИЙСК', 'НОВОРОССИЙСК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028105, 13317, 63, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦДПО на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028289, -1028208, 24, 'Республика Башкортостан на территории Горьковской дороги', 'Республика Башкортостан на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028110, 13317, 92, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦДПО на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027898, 7383, 76, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦФТО на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51540, -1000121, NULL, NULL, 'ЕЙСК', 'ЕЙСК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027726, -1027666, 92, 'По вине: Сервисные локомотивные депо на территории дороги: Восточно-Сибирская', 'СЛД (Локотех и СТМ) на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027901, 7383, 88, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦФТО на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027820, 2645, 83, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦВ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028276, -1028198, 1, 'Калининградская область на территории Октябрьской дороги', 'Калининградская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027609, 6427, 88, 'Красноярская,ДКСС ОАО "РЖД"', 'Красноярская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028191, NULL, NULL, 'Московская область', 'Московская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027855, 2678, 94, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Забайкальская', 'ЦШ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2679, NULL, NULL, 'ПКБ ЦТ', 'ПКБ ЦТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027998, 13006, 92, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦТ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027532, 13419, 24, 'Горьковская,ЦДИ ОАО "РЖД"', 'Горьковская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028364, -1028256, 88, 'Красноярский край на территории Красноярской дороги', 'Красноярский край на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027920, 7390, 96, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ТЭ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51530, -1000120, NULL, NULL, 'ЗАРЕЧНАЯ (ЭКСП.)', 'ЗАРЕЧНАЯ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028357, -1028251, 83, 'Алтайский край на территории Западно-сибирской дороги', 'Алтайский край на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7383, NULL, NULL, 'ЦФТО', 'ЦФТО OAO "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (96753, -1000164, NULL, NULL, 'ВАНИНО НСУ 2', 'ВАНИНО НСУ 2');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028183, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦЭУ, ЦУУ, ЦБА', 'ЦЭУ, ЦУУ, ЦБА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028217, NULL, NULL, 'Свердловская область', 'Свердловская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028160, 15508, 96, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Дальневосточная', 'ЦДИМ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2013, NULL, NULL, 'Сервисные организации', 'Сервисные организации');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027991, 13006, 58, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦТ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028284, -1028205, 17, 'Рязанская область на территории Московской дороги', 'Рязанская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027701, NULL, NULL, 'НОД-4 ЮУР', 'НОД-4 ЮУР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028164, 15773, 24, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Горьковская', 'ЦДМ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51130, -1000113, NULL, NULL, 'ЗАРЕЧНАЯ (ПЕРЕВ.)', 'ЗАРЕЧНАЯ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028001, 13015, 1, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦМ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027538, 7230, 28, 'Северная,ДКРС', 'Северная,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027690, NULL, NULL, 'Итого по сторонним пригородного комплекса', 'Итого по сторонним пригородного комплекса');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028114, 13922, 10, 'По вине: ООО "Локотех-Сервис" на территории дороги: Калининградская', 'ООО "Локотех-Сервис" на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028278, -1028200, 17, 'Владимирская область на территории Московской дороги', 'Владимирская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13211, NULL, NULL, 'Перевозчик Аэро-Экспресс', 'Перевозчик Аэро-Экспресс');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -9103, NULL, NULL, 'ЦБЗ', 'ЦБЗ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027591, -3619, 80, 'Южно-Уральская,ТЭ', 'Южно-Уральская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028198, NULL, NULL, 'Калининградская область', 'Калининградская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98490, -1000180, NULL, NULL, 'ХМЫЛОВСКИЙ (РЗД) (ЭКСП.)', 'ХМЫЛОВСКИЙ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52300, -1000137, NULL, NULL, 'ТЕМРЮК', 'ТЕМРЮК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028332, -1028238, 61, 'Саратовская область на территории Приволжской дороги', 'Саратовская область на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51150, -1000115, NULL, NULL, 'ТАГАНРОГ', 'ТАГАНРОГ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028151, 15508, 58, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Юго-Восточная', 'ЦДИМ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028285, -1028206, 17, 'Смоленская область на территории Московской дороги', 'Смоленская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3294, -1000087, NULL, NULL, 'ШУШАРЫ-С-П-П-ВИТ', 'ШУШАРЫ-С-П-П-ВИТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028168, 15773, 61, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Приволжская', 'ЦДМ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027822, 2645, 92, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦВ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000038, -2003, 28, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2016, NULL, NULL, 'Пассажирские', 'Пассажирские');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028053, 13029, 28, 'По вине: ВРК-2 на территории дороги: Северная', 'ВРК-2 на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027587, 8609, 76, 'Свердловская,ЦДРП ОАО "РЖД"', 'Свердловская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2634, NULL, NULL, 'ЦУКС', 'ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028367, -1028258, 92, 'Республика Бурятия на территории Восточно-сибирской дороги', 'Республика Бурятия на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028142, 14287, 92, 'По вине: ООО СТМ-Сервис на территории дороги: Восточно-Сибирская', 'ООО СТМ-Сервис на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027545, 6427, 41, 'СЕТЬ,ДКСС ОАО "РЖД"', 'СЕТЬ,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7384, NULL, NULL, 'ЦУНР', 'ЦУНР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7381, NULL, NULL, 'ДЕПАРТАМЕНТ ПО ВЗАИМОДЕЙСТВИЮ С ФЕДЕРАЛЬНЫМИ И РЕГИОНАЛЬНЫМИ ОРГАНАМИ ВЛАСТИ ОАО "РЖД"', 'ЦРВ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2006, NULL, NULL, 'Локомотиворемонтный завод', 'Локомотиворемонтный завод');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027529, 6427, 24, 'Горьковская,ДКСС ОАО "РЖД"', 'Горьковская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027779, 2597, 17, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦФ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51998, -1000126, NULL, NULL, 'КАВКАЗСКАЯ-ЧЕТ', 'КАВКАЗСКАЯ-ЧЕТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028330, -1028240, 61, 'Астраханская область на территории Приволжской дороги', 'Астраханская область на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13859, NULL, NULL, 'БУХГАЛТЕРСКАЯ СЛУЖБА ОАО "РЖД"', 'ЦБС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028249, NULL, NULL, 'Республика Казахстан', 'Республика Казахстан');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027510, -9998, 10, 'Калининградская,Прочие ОАО "РЖД"', 'Калининградская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028309, -1028223, 28, 'Ямало-Ненецкий автономный округ на территории Северной дороги', 'Ямало-Ненецкий автономный округ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000006, 2507, 28, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7456, -1000096, NULL, NULL, 'ЛУЖСКАЯ-ОТПРАВЛЕНИЕ', 'ЛУЖСКАЯ-ОТПР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028188, NULL, NULL, 'Республика Карелия', 'Республика Карелия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98000, -1000169, NULL, NULL, 'ВЛАДИВОСТОК', 'ВЛАДИВОСТОК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (41352, -1000106, NULL, NULL, 'ВЫСОЦКОЕ', 'ВЫСОЦКОЕ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027739, -1000000, 80, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Южно-Уральская', 'Холдинг на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027515, 8609, 10, 'Калининградская,ЦДРП ОАО "РЖД"', 'Калининградская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027892, 7383, 24, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦФТО на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2008, NULL, NULL, 'Вагоностроительный завод', 'Вагоностроительный завод');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027735, -1000000, 58, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Юго-Восточная', 'Холдинг на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027671, NULL, NULL, 'Предприятия, изготавливающие комплектующие узлы и детали для грузовых вагонов', 'Предприятия, изготавливающие комплектующие узлы и детали для грузовых вагонов');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027824, 2645, 96, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦВ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027686, NULL, NULL, 'Сервисные организации ДМВ', 'Сервисные организации ДМВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028354, -1028244, 80, 'Челябинская область на территории Южно-уральской дороги', 'Челябинская область на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7402, NULL, NULL, 'ДСЖТЮ', 'ДСЖТЮ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027833, 2646, 63, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ГВЦ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2007, NULL, NULL, 'Вагоноремонтный завод', 'Вагоноремонтный завод');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000008, 2507, 58, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7261, NULL, NULL, 'ЦРТП', 'ЦРТП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028212, NULL, NULL, 'Удмуртская Республика', 'Удмуртская Республика');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027530, 7230, 24, 'Горьковская,ДКРС', 'Горьковская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000044, -2003, 80, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027631, -3619, 96, 'Дальневосточная,ТЭ', 'Дальневосточная,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028202, NULL, NULL, 'Курская область', 'Курская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027761, 2433, 1, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦСС на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028092, 13316, 83, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦДМВ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027893, 7383, 28, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Северная', 'ЦФТО на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14627, NULL, NULL, 'АДМИНИСТРАТИВНО-ОРГАНИЗАЦИОННЫЙ АППАРАТ ОАО "РЖД"', 'ЦА ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027963, 8809, 80, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ДОСС на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028190, NULL, NULL, 'Ленинградская область', 'Ленинградская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028209, NULL, NULL, 'Республика Марий Эл', 'Республика Марий Эл');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028205, NULL, NULL, 'Рязанская область', 'Рязанская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027803, 2643, 80, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦД на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027595, 8609, 80, 'Южно-Уральская,ЦДРП ОАО "РЖД"', 'Южно-Уральская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027567, -3619, 61, 'Приволжская,ТЭ', 'Приволжская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028318, -1028232, 51, 'Ставропольский край на территории Северо-кавказской дороги', 'Ставропольский край на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028307, -1028221, 28, 'Костромская область на территории Северной дороги', 'Костромская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7670, -1000104, NULL, NULL, 'ЛУЖСКАЯ (ПАРОМ ЭКСП.)', 'ЛУЖСКАЯ-П-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028059, 13029, 80, 'По вине: ВРК-2 на территории дороги: Южно-Уральская', 'ВРК-2 на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027923, 7398, 17, 'По вине: АО "ФПК" на территории дороги: Московская', 'АО "ФПК" на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028089, 13316, 63, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦДМВ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027694, NULL, NULL, 'Вагоностроительные заводы (пасс)', 'Вагоностроительные заводы (пасс)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027625, 6427, 94, 'Забайкальская,ДКСС ОАО "РЖД"', 'Забайкальская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98780, -1000196, NULL, NULL, 'ПОСЬЕТ (ЭКСП.)', 'ПОСЬЕТ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028113, 13922, 1, 'По вине: ООО "Локотех-Сервис" на территории дороги: Октябрьская', 'ООО "Локотех-Сервис" на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000041, -2003, 61, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028300, -1028217, 24, 'Свердловская область на территории Горьковской дороги', 'Свердловская область на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (96780, -1000168, NULL, NULL, 'ВАНИНО (ЭКСП.)', 'ВАНИНО-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027615, -3619, 92, 'Восточно-Сибирская,ТЭ', 'Восточно-Сибирская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028120, 13922, 61, 'По вине: ООО "Локотех-Сервис" на территории дороги: Приволжская', 'ООО "Локотех-Сервис" на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027594, 7230, 80, 'Южно-Уральская,ДКРС', 'Южно-Уральская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52860, -1000151, NULL, NULL, 'КАВКАЗ-ПАРОМ-ЭКСПОРТ-НА-ВАРНУ', 'КАВКАЗ-ПАР-Э-ВАР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 6747, NULL, NULL, 'ЦКИ', 'ЦКИ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028125, 13922, 88, 'По вине: ООО "Локотех-Сервис" на территории дороги: Красноярская', 'ООО "Локотех-Сервис" на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028047, 13028, 94, 'По вине: ВРК-1 на территории дороги: Забайкальская', 'ВРК-1 на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 12077, NULL, NULL, 'ЦТР', 'ЦТР ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000004, 2507, 17, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027787, 2597, 80, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦФ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027770, 2433, 76, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦСС на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 15508, NULL, NULL, 'ДИРЕКЦИЯ ПО ЭКСПЛУАТАЦИИ ПУТЕВЫХ МАШИН-СТРУКТУРНОЕ ПОДРАЗДЕЛЕНИЕ ЦЕНТРАЛЬНОЙ ДИРЕКЦИИ ИНФРАСТРУКТУРЫ-ФИЛИАЛА ОАО "РЖД"', 'ЦДИМ ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027627, 8609, 94, 'Забайкальская,ЦДРП ОАО "РЖД"', 'Забайкальская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027799, 2643, 58, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦД на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028256, NULL, NULL, 'Красноярский край', 'Красноярский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027740, -1000000, 83, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Западно-Сибирская', 'Холдинг на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (48471, -1000108, NULL, NULL, 'АЗОВСТАЛЬ', 'АЗОВСТАЛЬ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028350, -1028248, 80, 'Курганская область на территории Южно-уральской дороги', 'Курганская область на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028139, 14287, 80, 'По вине: ООО СТМ-Сервис на территории дороги: Южно-Уральская', 'ООО СТМ-Сервис на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028195, NULL, NULL, 'Тверская область', 'Тверская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027650, -1000001, 88, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027838, 2646, 92, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ГВЦ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027858, 2686, 10, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Калининградская', 'ЦП на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027860, 2686, 24, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Горьковская', 'ЦП на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52320, -1000139, NULL, NULL, 'ВЫШЕСТЕБЛИЕВСКАЯ', 'ВЫШСТБЛ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028231, NULL, NULL, 'Краснодарский край', 'Краснодарский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028148, 15508, 24, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Горьковская', 'ЦДИМ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027938, 7728, 10, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ДЖВ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027565, 13861, 58, 'Юго-Восточная,ДКРЭ', 'Юго-Восточная,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027719, -1027666, 58, 'По вине: Сервисные локомотивные депо на территории дороги: Юго-Восточная', 'СЛД (Локотех и СТМ) на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028280, -1028202, 17, 'Курская область на территории Московской дороги', 'Курская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3292, -1000085, NULL, NULL, 'ШУШАРЫ-КУПЧ', 'ШУШАРЫ-КУПЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028166, 15773, 51, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Северо-Кавказская', 'ЦДМ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028290, -1028209, 24, 'Республика Марий Эл на территории Горьковской дороги', 'Республика Марий Эл на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027949, 7728, 88, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ДЖВ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028204, NULL, NULL, 'Орловская область', 'Орловская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028274, -1028196, 1, 'г. Москва на территории Октябрьской дороги', 'г. Москва на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98779, -1000195, NULL, NULL, 'ПОСЬЕТ САИПС', 'ПОСЬЕТ-САИПС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027743, -1000000, 94, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Забайкальская', 'Холдинг на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027684, NULL, NULL, 'Cервисные организации ЦТР', 'Cервисные организации ЦТР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027795, 2643, 17, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Московская', 'ЦД на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028229, NULL, NULL, 'Республика Северная Осетия-Алания', 'Республика Северная Осетия-Алания');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000040, -2003, 58, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 63, NULL, NULL, 'Куйбышевская', 'Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027882, 7280, 76, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Свердловская', 'ДМО на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027888, 7280, 96, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ДМО на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028027, 13022, 80, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦДТВ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 1, NULL, NULL, 'Октябрьская', 'Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028228, NULL, NULL, 'Карачаево-Черкесская республика', 'Карачаево-Черкесская республика');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028031, 13022, 94, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦДТВ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027968, 8809, 96, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ДОСС на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51070, -1000111, NULL, NULL, 'ЗАРЕЧНАЯ', 'ЗАРЕЧНАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027517, 13861, 10, 'Калининградская,ДКРЭ', 'Калининградская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2009, NULL, NULL, 'Частные ВРП', 'Частные ВРП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028136, 14287, 61, 'По вине: ООО СТМ-Сервис на территории дороги: Приволжская', 'ООО СТМ-Сервис на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028238, NULL, NULL, 'Саратовская область', 'Саратовская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028150, 15508, 51, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Северо-Кавказская', 'ЦДИМ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028338, -1028205, 63, 'Рязанская область на территории Куйбышевской дороги', 'Рязанская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3300, -1000089, NULL, NULL, 'ШУШАРЫ', 'ШУШАРЫ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98480, -1000179, NULL, NULL, 'НАХОДКА (ЭКСП.) УГОЛЬ', 'НАХОДКА-Э-УГ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027956, 8809, 24, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Горьковская', 'ДОСС на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2004, NULL, NULL, 'ООО TMX-Сервис', 'ООО TMX-Сервис');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2000, NULL, NULL, 'ЦДИДМ', 'ЦДИДМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2812, NULL, NULL, 'Вагонный комплекс', 'Вагонный комплекс');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027724, -1027666, 83, 'По вине: Сервисные локомотивные депо на территории дороги: Западно-Сибирская', 'СЛД (Локотех и СТМ) на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1850, -1000072, NULL, NULL, 'МУРМАНСК (ЭКСП.)', 'МУРМАНСК-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028215, NULL, NULL, 'Кировская область', 'Кировская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027710, NULL, NULL, 'Внешнее воздействие', 'Внешнее воздействие');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028158, 15508, 92, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Восточно-Сибирская', 'ЦДИМ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7650, -1000102, NULL, NULL, 'ЛУЖСКАЯ (ПЕРЕВ.)', 'ЛУЖСКАЯ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027750, 2391, 51, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦЛ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 8810, NULL, NULL, 'ЦБСФ', 'ЦБСФ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2624, NULL, NULL, 'ЦБТ', 'ЦБТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028161, 15773, 1, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Октябрьская', 'ЦДМ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027916, 7390, 83, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ТЭ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027953, 8809, 1, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ДОСС на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027878, 7280, 51, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ДМО на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027555, 8609, 51, 'Северо-Кавказская,ЦДРП ОАО "РЖД"', 'Северо-Кавказская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (50492, -1000109, NULL, NULL, 'УРАЛО-КАВКАЗСКАЯ', 'УРАЛО-КАВКАЗ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7345, NULL, NULL, '"ТрансКонтейнер', '"ТрансКонтейнер');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027894, 7383, 51, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦФТО на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028067, 13030, 17, 'По вине: ВРК-3 на территории дороги: Московская', 'ВРК-3 на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028069, 13030, 28, 'По вине: ВРК-3 на территории дороги: Северная', 'ВРК-3 на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027917, 7390, 88, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Красноярская', 'ТЭ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1460, -1000064, NULL, NULL, 'БЕЛОЕ МОРЕ (ЭКСП.)', 'БЕЛОЕ М-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027975, 12077, 58, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦТР на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027688, NULL, NULL, 'Машиностроительные заводы (МВПС)', 'Машиностроительные заводы (МВПС)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000020, -2507, 17, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028262, NULL, NULL, 'Приморский край', 'Приморский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028230, NULL, NULL, 'Чеченская республика', 'Чеченская республика');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027539, 8609, 28, 'Северная,ЦДРП ОАО "РЖД"', 'Северная,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7632, -1000099, NULL, NULL, 'ЛУЖСКАЯ-НЕФТЯНАЯ', 'ЛУЖСКАЯ-НЕФТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028180, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦУКС, ДКРС, ДКСС', 'ЦУКС, ДКРС, ДКСС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027955, 8809, 17, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Московская', 'ДОСС на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3590, -1000095, NULL, NULL, 'НОВЫЙ ПОРТ (ЭКСП.)', 'НОВЫЙ ПОРТ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028016, 13015, 96, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦМ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028201, NULL, NULL, 'Калужская область', 'Калужская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028200, NULL, NULL, 'Владимирская область', 'Владимирская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028361, -1028254, 83, 'Томская область на территории Западно-сибирской дороги', 'Томская область на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027700, NULL, NULL, 'ВСЕГО по транспортно-логистическому комплексу', 'ВСЕГО по транспортно-логистическому комплексу');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028091, 13316, 80, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦДМВ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027942, 7728, 51, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ДЖВ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028147, 15508, 17, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Московская', 'ЦДИМ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53360, -1000161, NULL, NULL, 'ТУАПСЕ-СОРТИРОВОЧНАЯ (ПЕРЕВ.)', 'ТУАПСЕ-С-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027505, 6427, 1, 'Октябрьская,ДКСС ОАО "РЖД"', 'Октябрьская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98003, -1000171, NULL, NULL, 'ВЛАДИВОСТОК-НЕЧ', 'ВЛАДИВОСТОК-НЕЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2003, NULL, NULL, 'Сторонние организации по инфраструктурному комплексу', 'Сторонние организации по инфраструктурному комплексу');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1570, -1027639, NULL, NULL, 'КАНДАЛАКША (ЭКСП.)', 'КАНДАЛАК-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2012, -1000076, NULL, NULL, 'ВЫБОРГ-ПРИГ', 'ВЫБОРГ-ПРИГ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028355, -1028249, 80, 'Республика Казахстан на территории Южно-уральской дороги', 'Республика Казахстан на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027566, -9998, 61, 'Приволжская,Прочие ОАО "РЖД"', 'Приволжская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028126, 13922, 92, 'По вине: ООО "Локотех-Сервис" на территории дороги: Восточно-Сибирская', 'ООО "Локотех-Сервис" на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 14630, NULL, NULL, 'ЦУИП', 'ЦУИП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027616, 2634, 92, 'Восточно-Сибирская,ЦУКС', 'Восточно-Сибирская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027507, 8609, 1, 'Октябрьская,ЦДРП ОАО "РЖД"', 'Октябрьская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027626, 7230, 94, 'Забайкальская,ДКРС', 'Забайкальская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027871, 2686, 94, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Забайкальская', 'ЦП на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028140, 14287, 83, 'По вине: ООО СТМ-Сервис на территории дороги: Западно-Сибирская', 'ООО СТМ-Сервис на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028275, -1028197, 1, 'г. Санкт-Петербург на территории Октябрьской дороги', 'г. Санкт-Петербург на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2814, NULL, NULL, 'Пассажирский комплекс', 'Пассажирский комплекс');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027792, 2597, 96, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦФ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027628, 13419, 94, 'Забайкальская,ЦДИ ОАО "РЖД"', 'Забайкальская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028076, 13030, 83, 'По вине: ВРК-3 на территории дороги: Западно-Сибирская', 'ВРК-3 на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027977, 12077, 63, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦТР на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028295, -1028214, 24, 'Пермский край на территории Горьковской дороги', 'Пермский край на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028144, 14287, 96, 'По вине: ООО СТМ-Сервис на территории дороги: Дальневосточная', 'ООО СТМ-Сервис на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2625, NULL, NULL, 'РЖДС', 'РЖДС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027704, NULL, NULL, 'ВСЕГО ПО СТОРОННИМ И СЕРВИСНЫМ ОРГАНИЗАЦИЯМ', 'ВСЕГО ПО СТОРОННИМ И СЕРВИСНЫМ ОРГАНИЗАЦИЯМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027536, 2634, 28, 'Северная,ЦУКС', 'Северная,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028308, -1028222, 28, 'Ярославская область на территории Северной дороги', 'Ярославская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027731, -1000000, 17, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Московская', 'Холдинг на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027632, 2634, 96, 'Дальневосточная,ЦУКС', 'Дальневосточная,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027762, 2433, 10, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦСС на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027870, 2686, 92, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦП на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027523, 8609, 17, 'Московская,ЦДРП ОАО "РЖД"', 'Московская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 61, NULL, NULL, 'Приволжская', 'Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027859, 2686, 17, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Московская', 'ЦП на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027733, -1000000, 28, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Северная', 'Холдинг на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000005, 2507, 24, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028084, 13316, 24, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦДМВ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027763, 2433, 17, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Московская', 'ЦСС на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027816, 2645, 61, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Приволжская', 'ЦВ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028070, 13030, 51, 'По вине: ВРК-3 на территории дороги: Северо-Кавказская', 'ВРК-3 на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027534, -9998, 28, 'Северная,Прочие ОАО "РЖД"', 'Северная,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027670, NULL, NULL, 'Компании операторы, предприятия собственники подвижного состава', 'Компании операторы, предприятия собственники подвижного состава');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027570, 7230, 61, 'Приволжская,ДКРС', 'Приволжская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 15773, NULL, NULL, 'ДИРЕКЦИЯ ДИАГНОСТИКИ И МОНИТОРИНГА ИНФРАСТРУКТУРЫ-СТРУКТУРНОЕ ПОДРАЗДЕЛЕНИЕ ЦЕНТРАЛЬНОЙ ДИРЕКЦИИ ИНФРАСТРУКТУРЫ-ФИЛИАЛА ОАО "РЖД"', 'ЦДМ ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028184, NULL, NULL, '1-ая линия КЦ', '1-ая линия КЦ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -128126, NULL, NULL, 'Управление анализа и статистики', 'Управление анализа и статистики');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028083, 13316, 17, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦДМВ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000045, -2003, 83, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028218, NULL, NULL, 'Республика Коми', 'Республика Коми');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028365, -1028257, 88, 'Иркутская область на территории Красноярской дороги', 'Иркутская область на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2632, NULL, NULL, 'ЦСР', 'ЦСР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027837, 2646, 88, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ГВЦ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028003, 13015, 17, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦМ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027674, NULL, NULL, 'Заводы изготовители рельсов и стрелочных переводов', 'Заводы изготовители рельсов и стрелочных переводов');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027911, 7390, 58, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ТЭ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027755, 2391, 80, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦЛ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027905, 7390, 1, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ТЭ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027945, 7728, 63, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ДЖВ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028216, NULL, NULL, 'Нижегородская область', 'Нижегородская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028269, -1028191, 1, 'Московская область на территории Октябрьской дороги', 'Московская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028298, -1028216, 24, 'Нижегородская область на территории Горьковской дороги', 'Нижегородская область на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027672, NULL, NULL, 'Предприятия стран СНГ и Балтии (груз.)', 'Предприятия стран СНГ и Балтии (груз.)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028314, -1028228, 51, 'Карачаево-Черкесская республика на территории Северо-кавказской дороги', 'Карачаево-Черкесская республика на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2645, NULL, NULL, 'ЦВ', 'ЦВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027527, -3619, 24, 'Горьковская,ТЭ', 'Горьковская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028266, -1028188, 1, 'Республика Карелия на территории Октябрьской дороги', 'Республика Карелия на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027553, 6427, 51, 'Северо-Кавказская,ДКСС ОАО "РЖД"', 'Северо-Кавказская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000018, -2507, 1, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028245, NULL, NULL, 'Омская область', 'Омская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028178, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦОПР (2)', 'ЦОПР (2)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -20721, NULL, NULL, 'Центр внутреннего контроля "Желдорконтроль"', 'ЦЖДК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027531, 8609, 24, 'Горьковская,ЦДРП ОАО "РЖД"', 'Горьковская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028042, 13028, 76, 'По вине: ВРК-1 на территории дороги: Свердловская', 'ВРК-1 на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028272, -1028194, 1, 'Псковская область на территории Октябрьской дороги', 'Псковская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027827, 2646, 17, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Московская', 'ГВЦ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027549, 13861, 41, 'СЕТЬ,ДКРЭ', 'СЕТЬ,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027709, NULL, NULL, 'Др. сторонние и сервисные организации транспортно-логистического комплекса', 'Др. сторонние и сервисные организации транспортно-логистического комплекса');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028006, 13015, 51, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦМ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027577, 6427, 63, 'Куйбышевская,ДКСС ОАО "РЖД"', 'Куйбышевская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027722, -1027666, 76, 'По вине: Сервисные локомотивные депо на территории дороги: Свердловская', 'СЛД (Локотех и СТМ) на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000029, -2507, 83, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027775, 2433, 94, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦСС на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -135892, NULL, NULL, 'Центр по координации управления рисками и построению системы внутреннего контроля', 'ЦРВК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2005, NULL, NULL, 'Локомотивостроительный завод', 'Локомотивостроительный завод');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028073, 13030, 63, 'По вине: ВРК-3 на территории дороги: Куйбышевская', 'ВРК-3 на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98520, -1000183, NULL, NULL, 'ХМЫЛОВСКИЙ', 'ХМЫЛОВСКИЙ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027576, 2634, 63, 'Куйбышевская,ЦУКС', 'Куйбышевская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027868, 2686, 83, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦП на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027973, 12077, 28, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Северная', 'ЦТР на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028051, 13029, 17, 'По вине: ВРК-2 на территории дороги: Московская', 'ВРК-2 на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027846, 2678, 51, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦШ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028287, -1028207, 17, 'Тульская область на территории Московской дороги', 'Тульская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028270, -1028192, 1, 'Мурманская область на территории Октябрьской дороги', 'Мурманская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027514, 7230, 10, 'Калининградская,ДКРС', 'Калининградская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028369, -1028231, 92, 'Краснодарский край на территории Восточно-сибирской дороги', 'Краснодарский край на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2552, NULL, NULL, 'ЦОС', 'ЦОС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027919, 7390, 94, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ТЭ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2686, NULL, NULL, 'ЦП', 'ЦП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027687, NULL, NULL, 'Ремонтные организации (МВПС)', 'Ремонтные организации (МВПС)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027661, -1000001, 10, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027941, 7728, 28, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Северная', 'ДЖВ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027884, 7280, 83, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ДМО на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027599, -3619, 83, 'Западно-Сибирская,ТЭ', 'Западно-Сибирская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028340, -1028239, 63, 'Тамбовская область на территории Куйбышевской дороги', 'Тамбовская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027537, 6427, 28, 'Северная,ДКСС ОАО "РЖД"', 'Северная,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028081, 13316, 1, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦДМВ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028322, -1028236, 58, 'Воронежская область на территории Юго-восточной дороги', 'Воронежская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027971, 12077, 17, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Московская', 'ЦТР на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028197, NULL, NULL, 'г. Санкт-Петербург', 'г. Санкт-Петербург');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000033, -2507, 96, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028086, 13316, 51, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦДМВ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2623, NULL, NULL, 'ТИ', 'ТИ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028310, -1028224, 51, 'Республика Дагестан на территории Северо-кавказской дороги', 'Республика Дагестан на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2409, NULL, NULL, 'ЦР', 'ЦР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027988, 13006, 24, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦТ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027614, -9998, 92, 'Восточно-Сибирская,Прочие ОАО "РЖД"', 'Восточно-Сибирская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52790, -1000145, NULL, NULL, 'КАВКАЗ', 'КАВКАЗ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028115, 13922, 17, 'По вине: ООО "Локотех-Сервис" на территории дороги: Московская', 'ООО "Локотех-Сервис" на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027836, 2646, 83, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ГВЦ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027952, 7728, 96, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ДЖВ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027785, 2597, 63, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦФ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13029, NULL, NULL, 'ВРК-2', 'ВРК-2');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028375, -1028263, 96, 'Хабаровский край на территории Дальневосточной дороги', 'Хабаровский край на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027801, 2643, 63, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦД на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028107, 13317, 80, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦДПО на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028032, 13022, 96, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦДТВ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027912, 7390, 61, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Приволжская', 'ТЭ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028093, 13316, 88, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦДМВ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027951, 7728, 94, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ДЖВ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51160, -1000116, NULL, NULL, 'ТАГАНРОГ (ЭКСП.)', 'ТАГАНРОГ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027511, -3619, 10, 'Калининградская,ТЭ', 'Калининградская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53350, -1000160, NULL, NULL, 'ТУАПСЕ-СОРТИРОВОЧНАЯ СЛИВ', 'ТУАПСЕ-С-СЛИВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027521, 6427, 17, 'Московская,ДКСС ОАО "РЖД"', 'Московская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027554, 7230, 51, 'Северо-Кавказская,ДКРС', 'Северо-Кавказская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2559, NULL, NULL, 'ЦИ', 'ЦИ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027568, 2634, 61, 'Приволжская,ЦУКС', 'Приволжская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027579, 8609, 63, 'Куйбышевская,ЦДРП ОАО "РЖД"', 'Куйбышевская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028319, -1028233, 51, 'Ростовская область на территории Северо-кавказской дороги', 'Ростовская область на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 8770, NULL, NULL, 'ЦЕНТР ОРГАНИЗАЦИИ КОНКУРСНЫХ ЗАКУПОК-СТРУКТУРНОЕ ПОДРАЗДЕЛЕНИЕ ОАО "РЖД"', 'ЦКЗ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028260, NULL, NULL, 'Амурская область', 'Амурская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52799, -1000147, NULL, NULL, 'КАВКАЗ-НАПР-ВАРНА', 'КАВКАЗ-НАПР-ВАРНА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027925, 7398, 28, 'По вине: АО "ФПК" на территории дороги: Северная', 'АО "ФПК" на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 10, NULL, NULL, 'Калининградская', 'Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028335, -1028211, 63, 'Республика Татарстан на территории Куйбышевской дороги', 'Республика Татарстан на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028014, 13015, 92, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦМ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028061, 13029, 88, 'По вине: ВРК-2 на территории дороги: Красноярская', 'ВРК-2 на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027573, 13861, 61, 'Приволжская,ДКРЭ', 'Приволжская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2340, -1000081, NULL, NULL, 'ВЫБОРГ (ПЕРЕВ.)', 'ВЫБОРГ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98080, -1000172, NULL, NULL, 'МЫС-ЧУРКИН', 'МЫС-ЧУРКИН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028171, 15773, 80, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Южно-Уральская', 'ЦДМ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 9416, NULL, NULL, 'ПКТБ Н', 'ПКТБ Н');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028174, 15773, 92, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Восточно-Сибирская', 'ЦДМ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028339, -1028242, 63, 'Самарская область на территории Куйбышевской дороги', 'Самарская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027540, 13419, 28, 'Северная,ЦДИ ОАО "РЖД"', 'Северная,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000026, -2507, 63, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028008, 13015, 61, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦМ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028311, -1028225, 51, 'Республика Ингушетия на территории Северо-кавказской дороги', 'Республика Ингушетия на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028048, 13028, 96, 'По вине: ВРК-1 на территории дороги: Дальневосточная', 'ВРК-1 на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028378, -1028265, 96, 'Еврейская автономная область на территории Дальневосточной дороги', 'Еврейская автономная область на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027621, 13861, 92, 'Восточно-Сибирская,ДКРЭ', 'Восточно-Сибирская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98760, -1000193, NULL, NULL, 'ПОСЬЕТ (перев.)', 'ПОСЬЕТ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028347, -1028246, 76, 'Тюменская область на территории Свердловской дороги', 'Тюменская область на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028221, NULL, NULL, 'Костромская область', 'Костромская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52120, -1000135, NULL, NULL, 'НОВОРОССИЙСК (ПЕРЕВ.)', 'НОВОРОСС-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028129, 14287, 1, 'По вине: ООО СТМ-Сервис на территории дороги: Октябрьская', 'ООО СТМ-Сервис на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027575, -3619, 63, 'Куйбышевская,ТЭ', 'Куйбышевская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027847, 2678, 58, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦШ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027957, 8809, 28, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Северная', 'ДОСС на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027590, -9998, 80, 'Южно-Уральская,Прочие ОАО "РЖД"', 'Южно-Уральская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027629, 13861, 94, 'Забайкальская,ДКРЭ', 'Забайкальская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027929, 7398, 63, 'По вине: АО "ФПК" на территории дороги: Куйбышевская', 'АО "ФПК" на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98070, -1027644, NULL, NULL, 'МЫС-ЧУРКИН (ПЕРЕВ.)', 'МЫС ЧУР-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027782, 2597, 51, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦФ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98530, -1000184, NULL, NULL, 'РЫБНИКИ', 'РЫБНИКИ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000049, -2003, 96, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027589, 13861, 76, 'Свердловская,ДКРЭ', 'Свердловская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027584, 2634, 76, 'Свердловская,ЦУКС', 'Свердловская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028134, 14287, 51, 'По вине: ООО СТМ-Сервис на территории дороги: Северо-Кавказская', 'ООО СТМ-Сервис на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3580, -1000094, NULL, NULL, 'НОВЫЙ ПОРТ', 'НОВЫЙ ПОРТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51570, -1000122, NULL, NULL, 'СТАРОМИНСКАЯ-ЕЙСКАЯ', 'СТАРОМИНСКАЯ-ЕЙСК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027890, 7383, 10, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦФТО на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027551, -3619, 51, 'Северо-Кавказская,ТЭ', 'Северо-Кавказская,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028219, NULL, NULL, 'Архангельская область', 'Архангельская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13028, NULL, NULL, 'ВРК-1', 'ВРК-1');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028348, -1028247, 76, 'Ханты-Мансийский автономный округ Югра на территории Свердловской дороги', 'Ханты-Мансийский автономный округ Югра на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027891, 7383, 17, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Московская', 'ЦФТО на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028368, -1028259, 92, 'Забайкальский край на территории Восточно-сибирской дороги', 'Забайкальский край на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7400, NULL, NULL, 'ЦЕНТР ТЕХНИЧЕСКОГО АУДИТА ОАО "РЖД"', 'ЦТА ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2013, -1000077, NULL, NULL, 'ВЫБОРГ-ПИХ', 'ВЫБОРГ-ПИХ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027715, -1027666, 17, 'По вине: Сервисные локомотивные депо на территории дороги: Московская', 'СЛД (Локотех и СТМ) на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98620, -1000192, NULL, NULL, 'ХМЫЛОВСКИЙ (РЗД) (ПЕРЕВ.)', 'ХМЫЛОВСКИЙ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028226, NULL, NULL, 'Кабардино-Балкарская республика', 'Кабардино-Балкарская республика');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027571, 8609, 61, 'Приволжская,ЦДРП ОАО "РЖД"', 'Приволжская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028194, NULL, NULL, 'Псковская область', 'Псковская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028279, -1028201, 17, 'Калужская область на территории Московской дороги', 'Калужская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028273, -1028195, 1, 'Тверская область на территории Октябрьской дороги', 'Тверская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028122, 13922, 76, 'По вине: ООО "Локотех-Сервис" на территории дороги: Свердловская', 'ООО "Локотех-Сервис" на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028306, -1028215, 28, 'Кировская область на территории Северной дороги', 'Кировская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000003, 2507, 10, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028065, 13030, 1, 'По вине: ВРК-3 на территории дороги: Октябрьская', 'ВРК-3 на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 28, NULL, NULL, 'Северная', 'Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028320, -1028234, 58, 'Белгородская область на территории Юго-восточной дороги', 'Белгородская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027897, 7383, 63, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦФТО на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027559, -3619, 58, 'Юго-Восточная,ТЭ', 'Юго-Восточная,ТЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52810, -1000149, NULL, NULL, 'КАВКАЗ (ЭКСП.)', 'КАВКАЗ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028358, -1028252, 83, 'Кемеровская область на территории Западно-сибирской дороги', 'Кемеровская область на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000016, 2507, 94, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027842, 2678, 10, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Калининградская', 'ЦШ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027928, 7398, 61, 'По вине: АО "ФПК" на территории дороги: Приволжская', 'АО "ФПК" на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51999, -1000127, NULL, NULL, 'КАВКАЗСКАЯ-НЕЧЕТ', 'КАВКАЗСКАЯ-НЕЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027564, 13419, 58, 'Юго-Восточная,ЦДИ ОАО "РЖД"', 'Юго-Восточная,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51080, -1000112, NULL, NULL, 'АЗОВ', 'АЗОВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027922, 7398, 10, 'По вине: АО "ФПК" на территории дороги: Калининградская', 'АО "ФПК" на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028351, -1028241, 80, 'Оренбургская область на территории Южно-уральской дороги', 'Оренбургская область на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51170, -1000117, NULL, NULL, 'ТАГАНРОГ (ПЕРЕВ.)', 'ТАГАНРОГ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027718, -1027666, 51, 'По вине: Сервисные локомотивные депо на территории дороги: Северо-Кавказская', 'СЛД (Локотех и СТМ) на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027723, -1027666, 80, 'По вине: Сервисные локомотивные депо на территории дороги: Южно-Уральская', 'СЛД (Локотех и СТМ) на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027518, -9998, 17, 'Московская,Прочие ОАО "РЖД"', 'Московская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028132, 14287, 24, 'По вине: ООО СТМ-Сервис на территории дороги: Горьковская', 'ООО СТМ-Сервис на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028030, 13022, 92, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦДТВ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027728, -1027666, 96, 'По вине: Сервисные локомотивные депо на территории дороги: Дальневосточная', 'СЛД (Локотех и СТМ) на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000021, -2507, 24, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027683, NULL, NULL, 'Остальные сервисные ЦТ', 'Остальные сервисные ЦТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027695, NULL, NULL, 'Частные вагоноремонтные предприятия (пасс)', 'Частные вагоноремонтные предприятия (пасс)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027768, 2433, 61, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦСС на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028119, 13922, 58, 'По вине: ООО "Локотех-Сервис" на территории дороги: Юго-Восточная', 'ООО "Локотех-Сервис" на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028372, -1028260, 94, 'Амурская область на территории Забайкальской дороги', 'Амурская область на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027944, 7728, 61, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Приволжская', 'ДЖВ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028232, NULL, NULL, 'Ставропольский край', 'Ставропольский край');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027913, 7390, 63, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ТЭ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028299, -1028205, 24, 'Рязанская область на территории Горьковской дороги', 'Рязанская область на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13832, NULL, NULL, 'ЦРСУ', 'ЦРСУ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028040, 13028, 61, 'По вине: ВРК-1 на территории дороги: Приволжская', 'ВРК-1 на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027561, 6427, 58, 'Юго-Восточная,ДКСС ОАО "РЖД"', 'Юго-Восточная,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027823, 2645, 94, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Забайкальская', 'ЦВ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027933, 7398, 88, 'По вине: АО "ФПК" на территории дороги: Красноярская', 'АО "ФПК" на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027737, -1000000, 63, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Куйбышевская', 'Холдинг на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027995, 13006, 80, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦТ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027742, -1000000, 92, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Восточно-Сибирская', 'Холдинг на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027588, 13419, 76, 'Свердловская,ЦДИ ОАО "РЖД"', 'Свердловская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027867, 2686, 80, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Южно-Уральская', 'ЦП на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028055, 13029, 58, 'По вине: ВРК-2 на территории дороги: Юго-Восточная', 'ВРК-2 на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027548, 13419, 41, 'СЕТЬ,ЦДИ ОАО "РЖД"', 'СЕТЬ,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027839, 2646, 94, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ГВЦ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028360, -1028245, 83, 'Омская область на территории Западно-сибирской дороги', 'Омская область на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027825, 2646, 1, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ГВЦ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1500, -1027638, NULL, NULL, 'КАНДАЛАКША (ПЕРЕВ.)', 'КАНДАЛАК-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027853, 2678, 88, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Красноярская', 'ЦШ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028060, 13029, 83, 'По вине: ВРК-2 на территории дороги: Западно-Сибирская', 'ВРК-2 на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027667, NULL, NULL, 'Вагоноремонтные заводы (груз.)', 'Вагоноремонтные заводы (груз.)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028021, 13022, 28, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦДТВ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 24, NULL, NULL, 'Горьковская', 'Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027765, 2433, 28, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Северная', 'ЦСС на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028220, NULL, NULL, 'Ивановская область', 'Ивановская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027569, 6427, 61, 'Приволжская,ДКСС ОАО "РЖД"', 'Приволжская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028277, -1028199, 17, 'Брянская область на территории Московской дороги', 'Брянская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52100, -1000133, NULL, NULL, 'НОВОРОССИЙСК (ЭКСП.)', 'НОВОРОСС-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027987, 13006, 17, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦТ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7765, NULL, NULL, 'ЦБСБ', 'ЦБСБ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028050, 13029, 10, 'По вине: ВРК-2 на территории дороги: Калининградская', 'ВРК-2 на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027622, -9998, 94, 'Забайкальская,Прочие ОАО "РЖД"', 'Забайкальская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7382, NULL, NULL, 'ЦУДЗ', 'ЦУДЗ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -125570, NULL, NULL, 'Ситуационный центр мониторинга и управления чрезвычайными ситуациями - структурное подразделение ОАО "РЖД"', 'ЦЧС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2619, NULL, NULL, 'ЦО', 'ЦО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027776, 2433, 96, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦСС на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3290, -1000083, NULL, NULL, 'ШУШАРЫ (ЭКСП.)', 'ШУШАРЫ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027814, 2645, 51, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦВ на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027989, 13006, 28, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦТ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027807, 2643, 94, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦД на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027981, 12077, 88, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦТР на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1845, -1000070, NULL, NULL, 'МУРМАНСК-КОМ-МУР', 'МУРМАНСК-КОМ-МУР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028064, 13029, 96, 'По вине: ВРК-2 на территории дороги: Дальневосточная', 'ВРК-2 на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1490, -1000066, NULL, NULL, 'КАНДАЛАКША', 'КАНДАЛАКША');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027730, -1000000, 10, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Калининградская', 'Холдинг на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027937, 7728, 1, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ДЖВ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98500, -1000181, NULL, NULL, 'РЫБНИКИ-ПЕРЕВАЛКА', 'РЫБНИКИ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028267, -1028189, 1, 'Вологодская область на территории Октябрьской дороги', 'Вологодская область на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028323, -1028202, 58, 'Курская область на территории Юго-восточной дороги', 'Курская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028033, 13028, 1, 'По вине: ВРК-1 на территории дороги: Октябрьская', 'ВРК-1 на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027781, 2597, 28, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦФ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000043, -2003, 76, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027580, 13419, 63, 'Куйбышевская,ЦДИ ОАО "РЖД"', 'Куйбышевская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027541, 13861, 28, 'Северная,ДКРЭ', 'Северная,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 6427, NULL, NULL, 'ДКСС', 'ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028304, -1028189, 28, 'Вологодская область на территории Северной дороги', 'Вологодская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2843, NULL, NULL, 'ПКТБ ЦКИ', 'ПКТБ ЦКИ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027906, 7390, 10, 'По вине: ТЭ  филиал ОАО "РЖД" на территории дороги: Калининградская', 'ТЭ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52990, -1000154, NULL, NULL, 'КАВКАЗ-ПАРОМ-КЕРЧЬ', 'КАВКАЗ-ПАР-КЕРЧЬ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028210, NULL, NULL, 'Республика Мордовия', 'Республика Мордовия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028303, -1028200, 28, 'Владимирская область на территории Северной дороги', 'Владимирская область на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028193, NULL, NULL, 'Новгородская область', 'Новгородская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027794, 2643, 10, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦД на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027970, 12077, 10, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦТР на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (3510, -1000090, NULL, NULL, 'НОВЫЙ ПОРТ (ПЕРЕВ.)', 'НОВЫЙ ПОРТ-П');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028041, 13028, 63, 'По вине: ВРК-1 на территории дороги: Куйбышевская', 'ВРК-1 на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027745, 2391, 1, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Октябрьская', 'ЦЛ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027864, 2686, 61, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Приволжская', 'ЦП на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98580, -1027640, NULL, NULL, 'МЫС АСТАФЬЕВА (ПЕРЕВ.)', 'МЫС АСТ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028321, -1028235, 58, 'Волгоградская область на территории Юго-восточной дороги', 'Волгоградская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028343, -1028212, 76, 'Удмуртская Республика на территории Свердловской дороги', 'Удмуртская Республика на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (53340, -1000158, NULL, NULL, 'ТУАПСЕ-СОРТИРОВОЧНАЯ', 'ТУАПСЕ-СОРТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027783, 2597, 58, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦФ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027865, 2686, 63, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦП на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028096, 13316, 96, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦДМВ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027976, 12077, 61, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦТР на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027844, 2678, 24, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Горьковская', 'ЦШ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2643, NULL, NULL, 'ЦД', 'ЦД OAO "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51520, -1000119, NULL, NULL, 'ЕЙСК (ЭКСП.)', 'ЕЙСК-ЭКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2015, NULL, NULL, 'Ремонтные организации', 'Ремонтные организации');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027749, 2391, 28, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦЛ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027597, 13861, 80, 'Южно-Уральская,ДКРЭ', 'Южно-Уральская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027751, 2391, 58, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦЛ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027654, -1000001, 63, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028337, -1028237, 63, 'Пензенская область на территории Куйбышевской дороги', 'Пензенская область на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027875, 7280, 17, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Московская', 'ДМО на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028362, -1028249, 83, 'Республика Казахстан на территории Западно-сибирской дороги', 'Республика Казахстан на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 76, NULL, NULL, 'Свердловская', 'Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13006, NULL, NULL, 'ЦТ', 'ЦТ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98560, -1000187, NULL, NULL, 'МЫС АСТАФЬЕВА', 'МЫС АСТАФЬЕВА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028182, NULL, NULL, 'Специфичные оргструктуры для отчетности ARIS: ЦЗТ (ЦУШ)', 'ЦЗТ (ЦУШ)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7396, NULL, NULL, 'ЦЖДК', 'ЦЖДК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028004, 13015, 24, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦМ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027712, NULL, NULL, 'Сервисные организации ЦТ', 'Сервисные организации ЦТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028189, NULL, NULL, 'Вологодская область', 'Вологодская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000009, 2507, 61, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028037, 13028, 28, 'По вине: ВРК-1 на территории дороги: Северная', 'ВРК-1 на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000025, -2507, 61, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52340, -1000140, NULL, NULL, 'ВЫШЕСТЕБЛИЕВСКАЯ-ПЕРЕВАЛКА', 'ВЫШСТБЛ-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028246, NULL, NULL, 'Тюменская область', 'Тюменская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027818, 2645, 76, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Свердловская', 'ЦВ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027943, 7728, 58, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ДЖВ на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027655, -1000001, 61, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028019, 13022, 17, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦДТВ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028170, 15773, 76, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Свердловская', 'ЦДМ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (51030, -1000110, NULL, NULL, 'АЗОВ (ЭКСП.)', 'АЗОВ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028374, -1028262, 96, 'Приморский край на территории Дальневосточной дороги', 'Приморский край на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000032, -2507, 94, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027978, 12077, 76, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦТР на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52880, -1000152, NULL, NULL, 'КАВКАЗ-ПАРОМ-САМСУН-ЭКСПОРТ', 'КАВКАЗ-ПАР-САМС-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028241, NULL, NULL, 'Оренбургская область', 'Оренбургская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52800, -1000148, NULL, NULL, 'КАВКАЗ (ПАРОМ ЭКСП.)', 'КАВКАЗ-ПАР-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027934, 7398, 92, 'По вине: АО "ФПК" на территории дороги: Восточно-Сибирская', 'АО "ФПК" на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027652, -1000001, 80, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (96765, -1000166, NULL, NULL, 'ВАНИНО ПОП', 'ВАНИНО ПОП');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027747, 2391, 17, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Московская', 'ЦЛ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027848, 2678, 61, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Приволжская', 'ЦШ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027754, 2391, 76, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦЛ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027809, 2645, 1, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Октябрьская', 'ЦВ на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000022, -2507, 28, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028045, 13028, 88, 'По вине: ВРК-1 на территории дороги: Красноярская', 'ВРК-1 на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027604, 13419, 83, 'Западно-Сибирская,ЦДИ ОАО "РЖД"', 'Западно-Сибирская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027786, 2597, 76, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦФ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027711, NULL, NULL, 'Прочие ЦТ', 'Прочие ЦТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028159, 15508, 94, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Забайкальская', 'ЦДИМ на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027986, 13006, 10, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦТ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2444, NULL, NULL, 'ПКБ ЦВ', 'ПКБ ЦВ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027547, 8609, 41, 'СЕТЬ,ЦДРП ОАО "РЖД"', 'СЕТЬ,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7633, -1000100, NULL, NULL, 'ЛУЖСКАЯ-СОРТИРОВОЧНАЯ', 'ЛУЖСКАЯ-СОРТ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027617, 6427, 92, 'Восточно-Сибирская,ДКСС ОАО "РЖД"', 'Восточно-Сибирская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027716, -1027666, 24, 'По вине: Сервисные локомотивные депо на территории дороги: Горьковская', 'СЛД (Локотех и СТМ) на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027542, -9998, 41, 'СЕТЬ,Прочие ОАО "РЖД"', 'СЕТЬ,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027593, 6427, 80, 'Южно-Уральская,ДКСС ОАО "РЖД"', 'Южно-Уральская,ДКСС ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027685, NULL, NULL, 'Предприятия, изготавливающие комплектующие узлы и детали для локомотивов', 'Предприятия, изготавливающие комплектующие узлы и детали для локомотивов');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028066, 13030, 10, 'По вине: ВРК-3 на территории дороги: Калининградская', 'ВРК-3 на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027697, NULL, NULL, 'Итого по сторонним организациям пассажирского комплекса', 'Итого по сторонним организациям пассажирского комплекса');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028025, 13022, 63, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦДТВ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027960, 8809, 61, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Приволжская', 'ДОСС на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2646, NULL, NULL, 'ГВЦ', 'ГВЦ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027948, 7728, 83, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ДЖВ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2610, NULL, NULL, 'ЦДЗ', 'ЦДЗ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028325, -1028237, 58, 'Пензенская область на территории Юго-восточной дороги', 'Пензенская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2011, -1000075, NULL, NULL, 'ВЫБОРГ-НЕЧЕТНЫЙ', 'ВЫБОРГ-НЕЧ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028208, NULL, NULL, 'Республика Башкортостан', 'Республика Башкортостан');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028329, -1028207, 58, 'Тульская область на территории Юго-восточной дороги', 'Тульская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2017, NULL, NULL, 'ЦД по ОТН', 'ЦД по ОТН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028192, NULL, NULL, 'Мурманская область', 'Мурманская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028062, 13029, 92, 'По вине: ВРК-2 на территории дороги: Восточно-Сибирская', 'ВРК-2 на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027798, 2643, 51, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ЦД на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027506, 7230, 1, 'Октябрьская,ДКРС', 'Октябрьская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -2011, NULL, NULL, 'Изготовители комплектующих', 'Изготовители комплектующих');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027983, 12077, 94, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦТР на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027900, 7383, 83, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦФТО на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027840, 2646, 96, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ГВЦ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028206, NULL, NULL, 'Смоленская область', 'Смоленская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027946, 7728, 76, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ДЖВ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52000, -1000128, NULL, NULL, 'КАВКАЗСКАЯ', 'КАВКАЗСКАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027618, 7230, 92, 'Восточно-Сибирская,ДКРС', 'Восточно-Сибирская,ДКРС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028090, 13316, 76, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦДМВ на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 13821, NULL, NULL, 'ЦЭУ', 'ЦЭУ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027582, -9998, 76, 'Свердловская,Прочие ОАО "РЖД"', 'Свердловская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2436, NULL, NULL, 'ЦБЗ', 'ЦБЗ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52797, -1000146, NULL, NULL, 'КАВКАЗ-НАПР-САМСУН', 'КАВКАЗ-НАПР-САМСУН');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028116, 13922, 24, 'По вине: ООО "Локотех-Сервис" на территории дороги: Горьковская', 'ООО "Локотех-Сервис" на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2000, -1000073, NULL, NULL, 'ВЫБОРГ', 'ВЫБОРГ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027666, NULL, NULL, 'Сервисные локомотивные депо', 'СЛД (Локотех и СТМ)');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028225, NULL, NULL, 'Республика Ингушетия', 'Республика Ингушетия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2060, -1000079, NULL, NULL, 'ВЫСОЦК', 'ВЫСОЦК');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52520, -1000142, NULL, NULL, 'ВЫШЕСТЕБЛИЕВСКАЯ-ЭКСПОРТ', 'ВЫШСТБЛ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028155, 15508, 80, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Южно-Уральская', 'ЦДИМ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028173, 15773, 88, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Красноярская', 'ЦДМ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028352, -1028242, 80, 'Самарская область на территории Южно-уральской дороги', 'Самарская область на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027516, 13419, 10, 'Калининградская,ЦДИ ОАО "РЖД"', 'Калининградская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028152, 15508, 61, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Приволжская', 'ЦДИМ на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028222, NULL, NULL, 'Ярославская область', 'Ярославская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027962, 8809, 76, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Свердловская', 'ДОСС на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52091, -1000130, NULL, NULL, 'НОВОРОССИЙСК-ПАРК-А', 'НОВОРОССИЙСК-ПАРК-А');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027800, 2643, 61, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦД на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027821, 2645, 88, 'По вине: ЦВ ЦДИ ОАО "РЖД" на территории дороги: Красноярская', 'ЦВ на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000011, 2507, 76, 'По дорогам по вине ОАО "РЖД"', 'По дорогам по вине ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027780, 2597, 24, 'По вине: ЦФ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ЦФ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027805, 2643, 88, 'По вине: ЦД филиал ОАО "РЖД" на территории дороги: Красноярская', 'ЦД на тер дор: Красноярская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52780, -1000144, NULL, NULL, 'ТЕМРЮК (ЭКСП.)', 'ТЕМРЮК-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98448, -1000174, NULL, NULL, 'НАХОДКА-ПАРК-Е', 'НАХОДКА-ПАРК-Е');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028068, 13030, 24, 'По вине: ВРК-3 на территории дороги: Горьковская', 'ВРК-3 на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028345, -1028245, 76, 'Омская область на территории Свердловской дороги', 'Омская область на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028149, 15508, 28, 'По вине: ЦДИМ ЦДИ ОАО "РЖД"-дирекция по эксплуатации путевых машин на территории дороги: Северная', 'ЦДИМ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028255, NULL, NULL, 'Республика Хакасия', 'Республика Хакасия');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028034, 13028, 10, 'По вине: ВРК-1 на территории дороги: Калининградская', 'ВРК-1 на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000031, -2507, 92, 'По дорогам по вине ДЗО', 'По дорогам по вине ДЗО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027613, 13861, 88, 'Красноярская,ДКРЭ', 'Красноярская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028106, 13317, 76, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Свердловская', 'ЦДПО на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028163, 15773, 17, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Московская', 'ЦДМ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028101, 13317, 28, 'По вине: ЦДПО филиал ОАО "РЖД" на территории дороги: Северная', 'ЦДПО на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028236, NULL, NULL, 'Воронежская область', 'Воронежская область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028028, 13022, 83, 'По вине: ЦДТВ филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦДТВ на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027600, 2634, 83, 'Западно-Сибирская,ЦУКС', 'Западно-Сибирская,ЦУКС');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (52170, -1000136, NULL, NULL, 'ГРУШЕВАЯ', 'ГРУШЕВАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027982, 12077, 92, 'По вине: ЦТР филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦТР на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027940, 7728, 24, 'По вине: ДЖВ филиал ОАО "РЖД" на территории дороги: Горьковская', 'ДЖВ на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027753, 2391, 63, 'По вине: ЦЛ филиал ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦЛ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027835, 2646, 80, 'По вине: ГВЦ филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ГВЦ на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -3625, NULL, NULL, 'Желдоручет', 'Желдоручет');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028082, 13316, 10, 'По вине: ЦДМВ филиал ОАО "РЖД" на территории дороги: Калининградская', 'ЦДМВ на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (98550, -1000186, NULL, NULL, 'КРАБОВАЯ (ЭКСП.)', 'КРАБОВАЯ-Э');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027524, 13419, 17, 'Московская,ЦДИ ОАО "РЖД"', 'Московская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027849, 2678, 63, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Куйбышевская', 'ЦШ на тер дор: Куйбышевская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028223, NULL, NULL, 'Ямало-Ненецкий автономный округ', 'Ямало-Ненецкий автономный округ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028250, NULL, NULL, 'Республика Алтай', 'Республика Алтай');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027843, 2678, 17, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Московская', 'ЦШ на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (96770, -1000167, NULL, NULL, 'ВАНИНО (ПЕРЕВ.)', 'ВАНИНО-ПЕР');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027879, 7280, 58, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Юго-Восточная', 'ДМО на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027966, 8809, 92, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ДОСС на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000042, -2003, 63, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027635, 8609, 96, 'Дальневосточная,ЦДРП ОАО "РЖД"', 'Дальневосточная,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027883, 7280, 80, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Южно-Уральская', 'ДМО на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028000, 13006, 96, 'По вине: ЦТ филиал ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦТ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027856, 2678, 96, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Дальневосточная', 'ЦШ на тер дор: Дальневосточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 7401, NULL, NULL, 'ЦИССО', 'ЦИССО');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027581, 13861, 63, 'Куйбышевская,ДКРЭ', 'Куйбышевская,ДКРЭ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028043, 13028, 80, 'По вине: ВРК-1 на территории дороги: Южно-Уральская', 'ВРК-1 на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028327, -1028238, 58, 'Саратовская область на территории Юго-восточной дороги', 'Саратовская область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028127, 13922, 94, 'По вине: ООО "Локотех-Сервис" на территории дороги: Забайкальская', 'ООО "Локотех-Сервис" на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027863, 2686, 58, 'По вине: ЦП ЦДИ ОАО "РЖД" на территории дороги: Юго-Восточная', 'ЦП на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028286, -1028195, 17, 'Тверская область на территории Московской дороги', 'Тверская область на тер дор: Московская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027729, -1000000, 1, 'По вине: Функциональные филиалы ОАО "РЖД" и Дочерние или Зависимые общества на территории дороги: Октябрьская', 'Холдинг на тер дор: Октябрьская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028138, 14287, 76, 'По вине: ООО СТМ-Сервис на территории дороги: Свердловская', 'ООО СТМ-Сервис на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028130, 14287, 10, 'По вине: ООО СТМ-Сервис на территории дороги: Калининградская', 'ООО СТМ-Сервис на тер дор: Калининградская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027526, -9998, 24, 'Горьковская,Прочие ОАО "РЖД"', 'Горьковская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, 2629, NULL, NULL, 'ЦРБ', 'ЦРБ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028356, -1028250, 83, 'Республика Алтай на территории Западно-сибирской дороги', 'Республика Алтай на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027603, 8609, 83, 'Западно-Сибирская,ЦДРП ОАО "РЖД"', 'Западно-Сибирская,ЦДРП ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (1846, -1000071, NULL, NULL, 'МУРМАНСК-КОЛА', 'МУРМАНСК-КОЛА');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028349, -1028208, 80, 'Республика Башкортостан на территории Южно-уральской дороги', 'Республика Башкортостан на тер дор: Южно-Уральская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027854, 2678, 92, 'По вине: ЦШ ЦДИ ОАО "РЖД" на территории дороги: Восточно-Сибирская', 'ЦШ на тер дор: Восточно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (2014, -1000078, NULL, NULL, 'ВЫБОРГ-ВЕРХ', 'ВЫБОРГ-ВЕРХ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028165, 15773, 28, 'По вине: ЦШ ЦДИ ОАО "РЖД"- дирекция диагностики и мониторинга инфраструктуры на территории дороги: Северная', 'ЦДМ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027612, 13419, 88, 'Красноярская,ЦДИ ОАО "РЖД"', 'Красноярская,ЦДИ ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (7630, -1000097, NULL, NULL, 'ЛУЖСКАЯ', 'ЛУЖСКАЯ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027903, 7383, 94, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Забайкальская', 'ЦФТО на тер дор: Забайкальская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -128142, NULL, NULL, 'Центр организации скоростного и высокоскоростного сообщения', 'ЦВСМ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028317, -1028231, 51, 'Краснодарский край на территории Северо-кавказской дороги', 'Краснодарский край на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028005, 13015, 28, 'По вине: ЦМ филиал ОАО "РЖД" на территории дороги: Северная', 'ЦМ на тер дор: Северная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027658, -1000001, 28, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028265, NULL, NULL, 'Еврейская автономная область', 'Еврейская автономная область');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027679, NULL, NULL, 'Сервисные организации ЦШ', 'Сервисные организации ЦШ');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028074, 13030, 76, 'По вине: ВРК-3 на территории дороги: Свердловская', 'ВРК-3 на тер дор: Свердловская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1000035, -2003, 10, 'По дорогам по вине Сторонних организаций по инфраструктурному комплексу', 'По дорогам по вине Сторонних организаций по инфраструктуре');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027646, NULL, NULL, 'Сторонние организации всего', 'Сторонние организации всего');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1028324, -1028203, 58, 'Липецкая область на территории Юго-восточной дороги', 'Липецкая область на тер дор: Юго-Восточная');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027772, 2433, 83, 'По вине: ЦСС филиал ОАО "РЖД" на территории дороги: Западно-Сибирская', 'ЦСС на тер дор: Западно-Сибирская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027896, 7383, 61, 'По вине: ЦФТО филиал ОАО "РЖД" на территории дороги: Приволжская', 'ЦФТО на тер дор: Приволжская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027659, -1000001, 24, 'По дорогам вне ответственности ОАО "РЖД"', 'По дорогам вне ответственности ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027502, -9998, 1, 'Октябрьская,Прочие ОАО "РЖД"', 'Октябрьская,Прочие ОАО "РЖД"');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027958, 8809, 51, 'По вине: ДОСС филиал ОАО "РЖД" на территории дороги: Северо-Кавказская', 'ДОСС на тер дор: Северо-Кавказская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (NULL, -1027876, 7280, 24, 'По вине: ДМО филиал ОАО "РЖД" на территории дороги: Горьковская', 'ДМО на тер дор: Горьковская');

INSERT INTO stg_skim_org_t (bk, id, root_id, child_id, vname, name) 
VALUES (-9,22337E+18, -2147483648, -2147483648, -2147483648, 'Не задано', 'Не задано');

-- Import Data into table stg_skim_org_t from file D:\amenyashev\Desktop\1\skim_org_t.csv . Task successful and sent to worksheet.


CREATE TABLE stg_skim_unit_t ( id VARCHAR(26),
name VARCHAR(128));



INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('86', 'усл. вагоны');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('85', 'тонн брутто');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('16', 'млн ткм нетто');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('23', 'млрд. долл. США');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('71', 'ед/млн.поездо-км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('2', '%');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('26', 'руб.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('42', 'компл.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('72', 'дни');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('44', 'тыс.шт.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('68', 'тяг.ед.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('69', 'машин');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('3', '% к  пр году');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('35', 'чел.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('24', 'млрд. руб.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('41', 'млн. чел.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('15', 'м');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('32', 'тыс. чел.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('59', 'Лет');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('70', 'млн. тонн');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('30', 'тыс. ткм. бр.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('66', 'км. пути');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('81', 'ткм/ваг/сут');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('63', 'кВТ-ч/10тыс.ткм брутто');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('45', 'тыс.м_3');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('20', 'млн. ткм.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('46', 'шт.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('56', 'тыс. часов');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('17', 'млн. пасс-км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('60', 'Руб/ваг/сутки');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('22', 'млн.руб');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('55', 'поездо-часов');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('25', 'поездо-км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('31', 'тыс. тонн');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('11', 'коп./10 ткм');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('54', 'место');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('57', 'млрб.ткм.бр.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('14', 'лок-сутки');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('62', 'сек');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('4', 'ДФЭ тыс.ед.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('52', 'рублей за Евро');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('64', 'неисправных вагонов');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('65', 'вагон');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('21', 'млн. ткм. бр.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('43', 'км.нити');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('10', 'коп./10 прив.ткм');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('38', 'млн.тонн');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('5', 'ед.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('28', 'тонн');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('51', 'рублей за доллар США');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('7', 'км/сут');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('8', 'км/ч');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('6', 'км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('80', 'ДФЭ');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('83', 'прив ед');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('61', '%/сек');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('40', 'млрд.ткм');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('27', 'руб./т.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('36', 'сутки');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('73', 'случай');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('75', 'млн. поездо-км.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('49', 'млрд. пасс-км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('53', 'балл');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('19', 'млн. ткм брутто/1000 чел.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('58', 'тыс.ткм брутто/чел.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('82', 'тыс.руб/чел');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('47', 'руб./поездо-час');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('77', 'тыс.ткм-бр/чел');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('33', 'тыс.руб');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('84', 'тыс.ткм брутто на локомотив');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('12', 'коэф.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('50', 'долл./барр.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('67', 'тыс. куб.м');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('18', 'млн. руб.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('76', 'Кг.у.т./Гкал');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('48', 'млрд.ткм.');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('39', 'млн.ткм/сутки');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('37', 'тыс. тонн/cутки');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('78', 'ед/млн. поездо-км');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('74', 'тыс. ткм');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('34', 'час');

INSERT INTO stg_skim_unit_t (id, name) 
VALUES ('|~|NULL|~|', 'Не задано');


CREATE TABLE stg_skim_val_type_t ( id VARCHAR(26),
name VARCHAR(128));



INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('10', 'Нарастающий итог по месяцам с начала квартала');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('9', 'Нарастающий итог по суткам с начала квартала');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('|~|NULL|~|', 'Не задано');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('4', 'Нарастающий итог по суткам с начала месяца');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('1', 'Итог');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('5', 'Нарастающий итог по неделям с начала года');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('3', 'Нарастающий итог по суткам с начала года');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('2', 'Нарастающий итог по месяцам с начала года');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('7', 'Нарастающий итог по годам');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('6', 'Нарастающий итог по кварталам с начала года');

INSERT INTO stg_skim_val_type_t (id, name) 
VALUES ('8', 'Нарастающий итог с начала ведения данных в системе-источнике');



CREATE TABLE stg_skim_var_t ( var VARCHAR(26),
name VARCHAR(256));



INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2843_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_2_aff', 'Абсолютное отклонение факта к факту пр. года по грузообороту собственных вагонов в порожнем состоянии');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0123_ff', 'Отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2834_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2085', 'Выполнение расписания движения грузовых поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D', 'Погрузка грузов - ЗЕРНО');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_othr_abs', 'Динамика прочей экспортной погрузки, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2_abs', 'Динамика погрузки грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1514_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по средней доходной ставки по грузовым перевозкам');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4424', 'Количество фактов отклонений по вине дороги');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_abs', 'Динамика погрузки зерна, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2841_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике м погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_sr', 'Погрузка грузов - ХИМИКАТЫ И СОДА (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2020_ff', 'Отклонение факта к факту прошлого года по выполнению расписания приг поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ct_ff', 'Отклонение факта к факту пр. года по нарушениям безопасности движения, отнесенные на ответственность локомотивного комплекса ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3097', 'Балл для рейтинга железных дорог');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_12', 'Погрузка грузов - ПРОМ.СЫРЬЕ И ФОРМ. МАТ-ЛЫ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2860', 'Инфраструктура (выручка от услуг)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_1_aff', 'Абсолютное отклонение факта к факту пр. года по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_abs', 'Динамика погрузки химикатов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки металлолома');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0', 'Доходы от перевозочной деятельности в грузовых перевозках');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_all', 'Отправленные пассажиры всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4072', 'Динамическая   нагрузка   общего вагона    ( рабочего вагона) в грузовом движении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4423', 'Факты отклонений от расписания (задержек) грузовых поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_abs', 'Отправленные пассажиры дальнего следования, тыс.чел');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ff', 'Отклонение факта к факту пр. года по количеству отказов технических средств 1-2 категории по вине ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2837_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_sr', 'Динамика погрузки строительных грузов (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2831', 'Динамика экспортной погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2959', 'Всеми видами ремонта');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_per_aff', 'Абсолютное отклонение факта прошлого года к факту по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4427', 'Выполнение расписания движения пассажирских поездов по  прибытию на станции посадки/высадки пассажиров с учетом опозданий при поступлении на дорогу и влияния дороги на опоздания по сети');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3386', 'Среднесуточный пробег локомотива рабочего парка');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_othr_abs_aff', 'Отклонение факта пр. года к факту по динамике прочей экспортной погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_sr', 'Погрузка во внутрироссийском сообщении (ср. с н.м.)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_abs_afp', 'Абсолютное отклонение плана к факту оперативному по погрузки во внутрироссийском сообщении, абс. млн.тонн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005', 'Экспорт');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_1_ff', 'Отклонение факта к факту пр. года по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2021_fp', 'Отклонение факта к плану прошлого года по выполнению расписания приг поездов по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки нефтеналивныз грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_sr_fp', 'Отклонение факта от плана по погрузке, факт тыс.т.в среднем в сутки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2849_aff', 'Отклонение фактак факту  пр. года по доли доходов от транзита в общем объеме доходов от перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_abs', 'Динамика погрузки черных металлов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_per_ff', 'Отклонения факта к факту пр. года по доходам от грузовых перевозок по (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0012_aff', 'Абсолютное отклонение факта к факту пр. года по среднему весу грузового поезда');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4075', 'Рабочий парк локомотивов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_all_fp', 'Отклонение факта к плану по отравленным пассажирам всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки металлолома');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_sr_aff', 'Абсолютное отклонение по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_abs', 'Тарифный грузооборот (абс. млрд.ткм)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_fp', 'Отклонение факта к плану по погрузке');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_per_afp', 'Абсолютное отклонение факта к плану по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07', 'Погрузка грузов - РУДА ЖЕЛЕЗНАЯ И МАРГАНЦЕВАЯ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0124_aff', 'Абсолютное отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам в пути следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_abs_fp', 'Отклонение факта к плану по динамике погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('15', 'Участковая скорость в грузовом движении с учетом движения по многопарковым станциям');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2834_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки нефтеналивных грузов,');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04', 'Погрузка грузов - НЕФТЬ И НЕФТЕПРОДУКТЫ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2020_aff', 'Абсолютное отклонение факта к факту прошлого года по выполнению расписания приг поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9', 'Грузооборот (всего)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2763_aff', 'Абсолютное отклонение факта к факту пр. года по продолжительности отказов технических средств');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24', 'Погрузка грузов - ГРУЗЫ В КОНТЕЙНЕРАХ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_abs_fp', 'Отклонение факта к плану по динамике погрузки металлолома');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузка в адрес российский портов (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0015_afp', 'Абсолютное отклонение факта к плану по участковой скорости');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_sr', 'Погрузка грузов - КАМЕННЫЙ УГОЛЬ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9_abs_afp', 'Абсолютное отклонение факта от плана по грузообороту (всего),  млрд.тн.км.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003', 'Погрузка в адрес портов Северо-Кавказской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2957', 'Количество событий по вине локомотивного комплекса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2022', 'Выполнение расписания приг поездов по отправлению с промежуточных пунтов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2845', 'Динамика экспортной погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2831_abs_ff', 'Отклонение факта пр. года к факту по экспортной динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2021_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пригородным поездам по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2846_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_all', 'Пассажирооборот всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_03', 'Погрузка грузов - КОКС');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2846', 'Динамика внутрироссийской погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2013_aff', 'Абсолютное отклонение факта к факту пр. года по "Количество фактов опозданий по сети по станциям назначения"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки строительных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4420', 'Факты задержек пригородных поездов по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_per', 'Доходы от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2021_aff', 'Абсолютное отклонение факта к факту прошлого года по выполнению расписания приг поездов по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_abs_aff', 'Абсолютное отклонение факта пр. года к факту оперативному по экспорту, абс. млн.тонн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0_abs_ff', 'Отклонение факта к факту пр. года по доходам от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_sr_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес российских портов (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2838_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2840', 'Динамика внутрироссийской погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F', 'Погрузка грузов - ХИМИЧЕСКИЕ И МИН. УДОБРЕНИЯ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2964', 'РИС - Сплошная смены рельсов новыми в объеме среднего ремонта');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2836', 'Динамика внутрироссийской погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2838', 'Динамика внутрироссийской погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2012_aff', 'Абсолютное отклонение факта к факту пр. года по "Количество фактов опозданий по сети по станциям посадки/высадки"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_all_ff', 'Отклонение факта к факту прошлого года по  пассажирообороту всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0_abs_fp', 'Отклонение факта к плану по доходам от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_abs_ff', 'Отклонение факта пр. года к факту оперативному по погрузки во внутрироссийском сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B', 'Погрузка грузов - ЛОМ ЧЕРНЫХ МЕТАЛЛОВ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_abs', 'Динамика погрузки нефтеналивных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0_abs_aff', 'Абсолютное отклонение факта к факту пр. года по доходам от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2849', 'Доля транзита, в  объеме доходов грузовых перевозок по факту пр. года');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес портов Дальневосточной ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2_abs_fp', 'Отклонение факта к плану по динамике погрузки грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2845_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0012_fp', 'Отклонение факта к плану по среднему весу грузового поезда');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2840_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2848_aff', 'Отклонение фактак факту  пр. года по доли транзита в общем объеме перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_abs_fp', 'Отклонение факта к плану по динамике погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_ff', 'Отклонение факта пр. года к факту по погрузке');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки химикатов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2', 'Погрузка грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0123_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пассажирским поездам по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2843_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_1_fp', 'Отклонение факта к плану по пассажирообороту дальнего следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2023_afp', 'Отклонение факта к плану (целевому значению) по пригородным поездам по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_othr_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике прочей внутрироссийской погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_abs_aff', 'Абсолютное отклонение факта к факту пр года по динамике прочей погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2023_aff', 'Абсолютное отклонение факта к факту прошлого года по выполнению расписания приг поездов по прибытию в пункты назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки химикатов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2728', 'Приведших к задержкам поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_sr', 'Погрузка в адрес портов Дальневосточной ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2968', 'Поставка щебня');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2020_fp', 'Отклонение факта к плану прошлого года по выполнению расписания приг поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_abs', 'Динамика погрузки руды железной, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2831_abs', 'Динамика экспортной погрузки угля, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_sr', 'Погрузка грузов - ЧЕРНЫЕ МЕТАЛЛЫ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес российский портов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2839_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0123_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес портов Северо-Кавказской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ctr_aff', 'Абсолютное отклонение факта к факту пр. года по нарушениям безопасности движения, отнесенные на ответственность локомотивного комплекса ЦТР');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3_abs_fp', 'Отклонение факта к плану по динамике погрузки грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1_abs_fp', 'Отклонение факта к плану по динамике погрузки грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs', 'Доходы от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_abs', 'Динамика погрузки лесных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('124', 'Выполнение расписания пассажирских поездов по станциям посадки/высадки в пути следования поезда');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0050_ff', 'Отклонение факта к факту пр.года по производительности локомотива');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2841_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2833', 'Динамика экспортной погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_abs_aff', 'Абсолютное отклоненение факта к факту прошлого года по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_sr', 'Динамика погрузки зерна (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3098', 'Место железной дороги в рейтинге');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_abs_aff', 'Абсолютное отклонение факта пр. года к факту оперативному по погрузки во внутрироссийском сообщении, абс. млн.тонн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_abs_fp', 'Отклонение факта к плану по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0125_aff', 'Абсолютное отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_sr_ff', 'Отклонение по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_1', 'Грузооборот тарифный');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3259_sr', 'Тепловозы, эксплуатируемый парк в грузовом движении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2832_abs_ff', 'Отклонение факта пр. года к факту по внутрироссийской динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3389', 'Грузооборот брутто (Т-км брутто  в грузовом движении)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_afp', 'Абсолютное отклонение факта к плану по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2841_abs', 'Динамика экспортной погрузки строит. грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_1_abs', 'Пассажирооборот в дальнем следовании');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2965', 'С - средний ремонт пути для сдачи ремонтно путевых работ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4422', 'Факты задержек пригородных поездов по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004_ff', 'Отклонение факта от факта прошлого года по в адрес портов Октябрьской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2013_ff', 'Отклонение факта к факту пр. года по "Количество фактов опозданий по сети по станциям назначения"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_abs_fp', 'Отклонение факта к плану по динамике погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4426', 'Количество фактов прибытия на станции посадки (высадки) пассажиров в пути следования поезда всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_ff', 'Отклонение факта к факту прошлого года (целевому значению) по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_abs_afp', 'Абсолютное отклонение факта от плана по погрузке,  млн.т');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1', 'Доходы от грузовых перевозок (по раскредитованию грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0124_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам в пути следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_abs', 'Погрузка грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2842_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес портов Дальневосточной ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2085_aff', 'Абсолютное отклонение факта к факту прошлого года по выполнению расписания движения грузовых поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2963', 'КРС - Капитальный ремонт пути на старогодных материалах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_abs_fp', 'Отклонение плана к факту по динамике погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_08', 'Погрузка грузов - РУДА ЦВ. И СЕРНОЕ СЫРЬЕ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ct', 'Количество отказов технических средств 1-2 категории по вине ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1514_sr_ff', 'Отклонение факта от факта прошлого года по средней доходной ставки по грузовым перевозкам');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_sr', 'Погрузка грузов (ср. с н.м.)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2840_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003_sr_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес портов Северо-Кавказской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_all_ff', 'Отклонение факта к факту прошлого года по отравленным пассажирам всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2958', 'События в т.ч. С пассажирскими локомотивами');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2839_abs', 'Динамика экспортной погрузки удобрений, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_abs_ff', 'Отклонение факта пр. года к факту по динамике прочей погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0015_fp', 'Отклонение факта к плану по участковой скорости');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0082_fp', 'Отклонение факта к плану по отправленным пассажирам пригородного сообщения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2022_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пригородным поездам по отправлению с промежуточных пунтов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1514_sr_fp', 'Отклонение факта от плана по средней доходной ставки по грузовым перевозкам');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки строительных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0124_ff', 'Отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам в пути следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_sr_fp', 'Отклонение факта к плану по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_14', 'Погрузка грузов - ЦЕМЕНТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2966', 'Поставка рельсов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_2_ff', 'Отклонение факта к факту пр. года по пассажирообороту пригородного сообщения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('81', 'Отправленные пассажиры дальнего следования, тыс.чел');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_abs_fp', 'Отклонение факта к плану по динамике погрузки химикатов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_abs', 'Динамика прочей погрузки, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2021_ff', 'Отклонение факта к факту прошлого года по выполнению расписания приг поездов по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_2_abs', 'Пассажирооборот в пригородном сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0015_ff', 'Отклонение факта к факту пр. года по участковой скорости');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_aff', 'Абсолютное отклонение факта от факта прошлого года по начисленной выручке (по отправлению грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004', 'Погрузка в адрес портов Октябрьской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4452', 'Эксплуатируемый парк Электровозы: Резерв ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2848', 'Доля транзита, в общем объеме перевозок по факту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2847_aff', 'Абсолютное отклонение факта к факту пр. года по динамике транзитных перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2832_abs_aff', 'Абсолютное отклонение факта к факту пр. года по внутрироссийской динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_abs_ff', 'Отклонение факта от факта прошлого года по погрузке,  млн.т');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2023_ff', 'Отклонение факта к факту прошлого года по выполнению расписания приг поездов по прибытию в пункты назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0125_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_ff', 'Отклонения факта к факту пр. года по доходам от грузовых перевозок по (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2833_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0_abs_ff', 'Отклонение факта к факту прошлого года (целевому значению) по выручке, начисленной по перевозочным видам деятельности от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_sr_ff', 'Отклонение факта от факта прошлого года по погрузке, факт тыс.т.в среднем в сутки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_abs', 'Динамика погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0_abs_aff', 'Абсолютное отклонение факта к факту прошлого года по выручке, начисленной по перевозочным видам деятельности от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9_abs_ff', 'Отклонение факта от факта прошлого года по грузообороту (всего),  млрд.тн.км.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_abs_fp', 'Тарифный грузооборот, прирост план млн.ткм');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4421', 'Факты задержек пригородных поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0124_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пассажирским поездам в пути следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_afp', 'Отклонение факта к плану по погрузке (абс.млн.тонн)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_2_ff', 'Отклонение факта пр. года к факту по грузообороту собственных вагонов в порожнем состоянии');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_sr', 'Погрузка грузов - ХИМИЧЕСКИЕ И МИН. УДОБРЕНИЯ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2763_ff', 'Отклонение факта к факту пр. года по продолжительности отказов технических средств');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003_sr', 'Погрузка в адрес портов Северо-Кавказской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3324_sr', 'Содержание парка грузовых электровозов по состоянию в ожидании работ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_abs_afp', 'Абсолютное отклонение плана к факту по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9_abs_aff', 'Абсолютное отклонение факта от факта прошлого года по грузообороту (всего),  млрд.тн.км.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_abs_ff', 'Отклонение факта пр. года к факту оперативному по экспорту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('12', 'Средний вес брутто поезда грузового движения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2835_abs', 'Динамика экспортной погрузки руды железной, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0125_ff', 'Отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_othr_abs', 'Динамика прочей внутрироссийской погрузки, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2834_abs', 'Динамика внутрироссийской погрузки нефтеналивных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2011', 'Количество фактов опозданий по сети по станциям отправления');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2011_aff', 'Абсолютное отклонение факта к факту пр. года по "Количество фактов опозданий по сети  по станциям отправления"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4419', 'Факты задержек пассажирских поездов по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2834', 'Динамика внутрироссийской погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_1', 'Пассажирооборот в дальнем следовании');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_per_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2844_abs', 'Динамика внутрироссийской погрузки зерна, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0_abs', 'Выручка, начисленная по перевозочным видам деятельности от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_abs_fp', 'Отклонение факта к плану по динамике погрузки строительных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2011_ff', 'Отклонение факта к факту пр. года по "Количество фактов опозданий по сети  по станциям отправления"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0_abs_afp', 'Абсолютное отклонение факта к плану по доходам от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513', 'Грузовые перевозки (выручка от услуг)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4428', 'Выполнение расписания движения пригородных поездов по прибытию в пункты назначения с учетом отклонений при поступлении на дорогу и влияния дороги на опоздания по сети (учитываемый показатель)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки строительных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1514_sr', 'Средняя доходная ставка по грузовым перевозкам');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2835_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr', 'Динамика прочей погрузки, тыс.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10', 'Погрузка грузов - ХИМИКАТЫ И СОДА');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_othr', 'Динамика прочей экспортной погрузки, тыс.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3388', 'Техническая скорость  поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2831_abs_aff', 'Абсолютное отклонение факта к факту пр. года по экспортной динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2847_ff', 'Отклонение факта к факту пр. года по динамике транзитных перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_sr', 'Тарифный грузооборот (ср. с н.м.)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2839', 'Динамика экспортной погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_per_afp', 'Абсолютное отклонение плана к факту по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке, факт тыс.т.в среднем в сутки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2844_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2836_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике внутрироссийской погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2847', 'Динамика транзитных перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес российских портов (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_fp', 'Отклонение факта к плану по отправленным пассажирам дальнего следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0050_fp', 'Отклонение факта к плану по производительности локомотива');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_fp', 'Отклонение факта от плана по начисленной выручка (по отправлению грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_abs', 'Экспорт, абс.млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2022_aff', 'Абсолютное отклонение факта к факту прошлого года по выполнению расписания приг поездов по отправлению с промежуточных пунтов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2845_abs', 'Динамика экспортной погрузки лесных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2833_abs', 'Динамика экспортной погрузки нефтеналивных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2022_ff', 'Отклонение факта к факту прошлого года по выполнению расписания приг поездов по отправлению с промежуточных пунтов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2012_ff', 'Отклонение факта к факту пр. года по "Количество фактов опозданий по сети по станциям посадки/высадки"');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_abs', 'Динамика погрузки угля, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2842_abs', 'Динамика внутрироссийской погрузки строит. грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_afp', 'Абсолютное отклонение плана к факту по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0012_ff', 'Отклонение факта к факту пр. года по среднему весу грузового поезда');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_abs_fp', 'Отклонение факта к плану по динамике погрузки улля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_othr', 'Динамика прочей внутрироссийской погрузки, тыс.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2020_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пригородным поездам по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_sr', 'Динамика погрузки грузов в контейнерах (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729', 'Количество отказов технических средств 1-2 категорий');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_abs', 'Погрузка во внутрироссийском сообщении, абс.млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0', 'Выручка, начисленная по перевозочным видам деятельности от грузовых перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_othr_abs_ff', 'Абсолютное отклонение факта к факту пр. года по динамике прочей экспортной погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_sr', 'Погрузка грузов - НЕФТЬ И НЕФТЕПРОДУКТЫ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2840_abs', 'Динамика внутрироссийской погрузки удобрений, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2842_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2025', 'Импорт через порты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2843', 'Динамика экспортной погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_abs_fp', 'Отклонение факта от плана по погрузке,  млн.т');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2012', 'Количество фактов опозданий по сети по станциям посадки/высадки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_2_fp', 'Отклонение факта к плану по грузообороту собственных вагонов в порожнем состоянии');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_aff', 'Абсолютное отклонение факта прошлого года к факту по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_abs_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке,  млн.т');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('82', 'Отправление пассажиров в пригородном сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2013', 'Количество фактов опозданий по сети по станциям назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09', 'Погрузка грузов - ЧЕРНЫЕ МЕТАЛЛЫ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_ff', 'Отклонение факта к факту пр. года по отправленным пассажирам дальнего следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all', 'Погрузка в адрес российский портов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ctr_ff', 'Отклонение факта к факту пр. года по нарушениям безопасности движения, отнесенные на ответственность локомотивного комплекса ЦТР');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0125_afp', 'Абсолютное отклонение факта к плану (целевому значению) по пассажирским поездам по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0_abs_afp', 'Абсолютное отклонение факта к плану по выручке, начисленной по перевозочным видам деятельности от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2837_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес портов Северо-Кавказской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004_sr', 'Погрузка в адрес портов Октябрьской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4418', 'Факты задержек пассажирских поездов в пути следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_all_fp', 'Отклонение факта к плану по  пассажирообороту всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2960', 'СПЖБ -Укладка новых и старогодных стрелочных переводов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_abs', 'Динамика погрузки удобрений, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4425', 'Общее количество фактов прибытия пригородных поездов в пункты назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004_sr_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес портов Октябрьской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2839_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2846_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2861', 'Пассажирские перевозки (выручка от услуг)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11_abs', 'Динамика погрузки строительных грузов, млн.тн.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2763', 'Продолжительность отказов технических средств');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_2_afp', 'Абсолютное отклонение факта к плану по грузообороту собственных вагонов в порожнем состоянии');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_per_aff', 'Абсолютное отклонение факта к факту прошлого года по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_1_fp', 'Отклонение факта к плану по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес портов Октябрьской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2967', 'Поставка шпал');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_1_ff', 'Отклонение факта к факту пр. года по пассажирообороту дальнего следования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3410', 'Маршрутная скорость груженых маршрутов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0F_abs_fp', 'Отклонение плана к факту по динамике погрузки удобрений');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002', 'Погрузка в адрес портов Дальневосточной ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3', 'Погрузка грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_per', 'Начисленная выручка (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_2', 'Пассажирооборот в пригородном сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_afp', 'Абсолютное отклонение факта от плана по начисленной выручка (по отправлению грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_othr_abs_ff', 'Отклонение факта пр. года к факту по динамике прочей внутрироссийской погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_2_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки грузов 2 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0015_aff', 'Абсолютное отклонение факта к факту пр. года по участковой скорости');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1514_sr_afp', 'Абсолютное отклонение факта от плана по средней доходной ставки по грузовым перевозкам');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2844_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2', 'Погрузка грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2845_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике экспортной погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки металлолома');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15', 'Погрузка грузов - ЛЕСНЫЕ ГРУЗЫ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ctr', 'Нарушения безопасности движения, отнесенные на ответственность локомотивного комплекса ЦТР');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_07_sr', 'Погрузка грузов - РУДА ЖЕЛЕЗНАЯ И МАРГАНЦЕВАЯ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2835_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2832_abs', 'Динамика внутрироссийской погрузки угля, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2848_ff', 'Отклонение фактак факту  пр. года по доли транзита в общем объеме перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_abs_fp', 'Отклонение плана к факту по динамике погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2004_sr_aff', 'Абсолютное отклонение факта от факта прошлого года по погрузке в адрес портов Октябрьской ж.д. (ср. с начала периода)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_abs', 'Динамика погрузки металлолома');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_all_abs', 'Пассажирооборот всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_per_ff', 'Отклонение факта к факту прошлого года (целевому значению) по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_sr_afp', 'Абсолютное отклонение факта от плана по погрузке, факт тыс.т.в среднем в сутки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2003_ff', 'Отклонение факта от факта прошлого года по в адрес портов Северо-Кавказской ж.д.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2021', 'Выполнение расписания приг поездов по прибытию на промежуточные пункты');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4076', 'Средний состав поезда в грузовом движении ( средняя длина поезда)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2022_fp', 'Отклонение факта к плану прошлого года по выполнению расписания приг поездов по отправлению с промежуточных пунтов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2085_ff', 'Отклонение факта к факту прошлого года по выполнению расписания движения грузовых поездов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_sr', 'Динамика погрузки лесных грузов (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0123_aff', 'Абсолютное отклонение факта к факту прошлого года (целевому значению) по пассажирским поездам по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2836_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_abs_fp', 'Отклонение плана к факту оперативному по экспорту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2020', 'Выполнение расписания приг поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0004_2_fp', 'Отклонение факта к плану по пассажирообороту пригородного сообщения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2849_ff', 'Отклонение фактак факту  пр. года по доли доходов от транзита в общем объеме доходов от перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2023', 'Выполнение расписания приг поездов по прибытию в пункты назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1_abs', 'Динамика погрузки грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs_aff', 'Абсолютное отклонение факта к факту прошлого года по начисленной выручке (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2837_abs', 'Динамика экспортной погрузки черных металлов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('125', 'Выполнение расписания пассажирских поездов по прибытию на станцию назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_sr', 'Экспорт (ср. с н.м.)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2023_fp', 'Отклонение факта к плану прошлого года по выполнению расписания приг поездов по прибытию в пункты назначения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2859', 'Объем доходов транзитных перевозок');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2835', 'Динамика экспортной погрузки руды железной');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4074', 'Процент порожнего пробега в грузовом движении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2837', 'Динамика экспортной погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0050_aff', 'Абсолютное отклонение факта к факту пр.года по производительности локомотива');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_aff', 'Абсолютное отклонение факта к факту пр. года по количеству отказов технических средств 1-2 категории по вине ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1', 'Погрузка грузов 1 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_15_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки лесных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2842', 'Динамика внутрироссийской погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2844', 'Динамика внутрироссийской погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all_sr', 'Погрузка в адрес российский портов (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3_abs', 'Динамика погрузки грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_sr', 'Динамика прочей погрузки (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2838_abs_ff', 'Отклонение факта пр. года к факту по динамике внутрироссийской погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9_abs_fp', 'Отклонение факта от плана по грузообороту (всего),  млрд.тн.км.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('123', 'Выполнение расписания пассажирских поездов по отправлению со станции формирования');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008_abs_fp', 'Отклонение плана к факту оперативному по погрузки во внутрироссийском сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_per_fp', 'Отклонения факта к плану по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес российский портов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0050_afp', 'Абсолютное отклонение факта к плану по производительности локомотива');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4417', 'Факты задержек пассажирских поездов по отправлению');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2843_abs', 'Динамика экспортной погрузки зерна, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_9_abs', 'Грузооборот (всего),  млрд.тн.км.');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02_abs_aff', 'Абсолютное отклонение факта пр. года к факту по динамике погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2005_abs_afp', 'Абсолютное отклонение плана к факту оперативному по экспорту, абс. млн.тонн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2962', 'Р/КРН -Реконструкция (модернизация) пути и капитальный ремонт пути на новых материалах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0012_afp', 'Абсолютное отклонение факта к плану по среднему весу грузового поезда');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2728_ff', 'Отклонение факта оперативного к факту пр. периода по количеству тыс.случаев');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0082_ff', 'Отклонение факта к факту пр. года по отправленным пассажирам пригородного сообщения');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2838_abs', 'Динамика внутрироссийской погрузки черных металлов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_ff', 'Отклонение факта от факта прошлого года по начисленной выручка (по отправлению грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_04_abs_ff', 'Отклонение факта пр. года к факту по динамике погрузки нефтеналивных грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2961', 'РИ - сплошная смена рельсов новыми');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('50', 'Среднесуточная производительность локомотива рабочего парка в грузовом движении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_1D_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки зерна');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2846_abs', 'Динамика внутрироссийской погрузки лесных грузов, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_09_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки черных металлов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2008', 'Внутренние перевозки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_10_abs_ff', 'Отклонение факта к факту пр. года по динамике погрузки химикатов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_02', 'Погрузка грузов - КАМЕННЫЙ УГОЛЬ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('4453', 'Эксплуатируемый парк Тепловозы: Резерв ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_abs_ff', 'Тарифный грузооборот, прирост факт млн.ткм');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_abs_fp', 'Отклонение факта к плану по динамике прочей погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_24_abs_afp', 'Абсолютное отклонение факта к плану по динамике погрузки грузов в контейнерах');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_0_abs_fp', 'Отклонение факта к плану (целевому значению) по пассажирским поездам по выручке, начисленной по перевозочным видам деятельности от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_sr_afp', 'Абсолютное отклонение факта к плану по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2836_abs', 'Динамика внутрироссийской погрузки руды железной, млн.тн');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_1_abs_fp', 'Отклонения факта к плану по доходам от грузовых перевозок (по раскредитованию грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0081_all_abs', 'Отправленные пассажиры всего');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_3_abs_aff', 'Абсолютное отклонение факта к факту пр. года по динамике погрузки грузов 3 класса');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_2', 'Грузооборот собственных вагонов в порожнем состоянии');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1_abs', 'Начисленная выручка (по отправлению грузов), млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2832', 'Динамика внутрироссийской погрузки угля');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_aff', 'Отклонение факта к факту пр.года по погрузке (абс.млн.тонн)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1513_0_abs', 'Доходы от грузовых перевозок, млрд.руб');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_0B_sr', 'Погрузка грузов - ЛОМ ЧЕРНЫХ МЕТАЛЛОВ (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_othr_abs_afp', 'Абсолютное отклонение факта к плану по динамике прочей погрузки');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1775', 'Количество тыс.случаев');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0002_11', 'Погрузка грузов - СТРОИТЕЛЬНЫЕ ГРУЗЫ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3257_sr', 'Электровозы, рабочий парк в грузовом движении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2002_all_sr_ff', 'Отклонение факта от факта прошлого года по погрузке в адрес российский портов (среднее за период)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2833_abs_ff', 'Отклонение факта пр. года к факту по динамике экспортной погрузки нефтеналивных грузов,');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2841', 'Динамика экспортной погрузки строит. грузов');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('2729_ct_aff', 'Абсолютное отклонение факта к факту пр. года по нарушениям безопасности движения, отнесенные на ответственность локомотивного комплекса ЦТ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('3325_sr', 'Содержание парка грузовых тепловозов по состоянию в ожидании работ');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0003_1_afp', 'Абсолютное отклонение факта к плану по тарифному грузообороту');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('0082_abs', 'Отправление пассажиров в пригородном сообщении');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('1512_1', 'Начисленная выручка (по отправлению грузов)');

INSERT INTO stg_skim_var_t (var, name) 
VALUES ('|~|NULL|~|', 'Не задано');