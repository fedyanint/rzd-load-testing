
CREATE TABLE DIM_skim_cargo_type (
	cargo_type varchar(4000) NOT null PRIMARY KEY,
	name varchar(4000) NULL
);


CREATE TABLE DIM_skim_data_type (
	id varchar(26) NOT null PRIMARY KEY,
	name varchar(26) NULL
);


CREATE TABLE DIM_skim_date_type (
	id int4 NOT null PRIMARY KEY,
	name varchar(26) NULL
);


CREATE TABLE DIM_skim_metric_type (
	id int4 NOT null PRIMARY KEY,
	name varchar(128) NULL
);


CREATE TABLE DIM_skim_org (
	bk int4 NULL,
	id int4 NOT null PRIMARY KEY,
	root_id int4 NULL,
	child_id int4 NULL,
	vname varchar(256) NULL,
	name varchar(128) NULL
);


CREATE TABLE DIM_skim_unit (
	id varchar(26) NOT null PRIMARY KEY,
	name varchar(128) NULL
);


CREATE TABLE DIM_skim_val_type (
	id varchar(26) NOT null PRIMARY KEY,
	name varchar(128) NULL
);


CREATE TABLE DIM_skim_var (
	var varchar(26) NOT null PRIMARY KEY,
	name varchar(256) NULL
);

insert into DIM_skim_cargo_type
select * from stg_skim_cargo_type_t;

insert into DIM_skim_data_type
select * from stg_skim_data_type_t;

insert into DIM_skim_date_type
select * from stg_skim_date_type;

insert into DIM_skim_metric_type
select * from stg_skim_metric_type_t;

insert into DIM_skim_org
select * from stg_skim_org_t;

insert into DIM_skim_unit
select * from stg_skim_unit_t;

insert into DIM_skim_val_type
select * from stg_skim_val_type_t;

insert into DIM_skim_var
select * from stg_skim_var_t;
