import os
from datetime import timedelta

from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable
from airflow.utils import dates


dag_params = {
    'dag_id': 'rzd_load_gp',
    'start_date': dates.days_ago(1),
    'schedule_interval': None
}


script_folder = os.path.join(os.path.dirname(__file__), 'sql')
postgres_conn_id = 'rzd_postgres_connect'
load_dds_script_path = os.path.join(script_folder, 'load_dm_all_indicators_v.sql')
load_id = Variable.get('LOAD_ID', default_var='0') or '0'

with DAG(**dag_params) as gp_dag:
    start = DummyOperator(task_id='rzd-gp-start-{}'.format(load_id),
                          retries=30,
                          retry_delay=timedelta(minutes=1))

    clean_tasks = []
    clean_task = PostgresOperator(task_id='clean-dm_all_indicators_stg',
                                  sql="alter table public.dm_all_indicators_stg truncate partition for(%s)",
                                  parameters=(int(load_id),),
                                  postgres_conn_id=postgres_conn_id,
                                  retries=30,
                                  retry_delay=timedelta(minutes=1))
    clean_tasks.append(clean_task)
    clean_task = PostgresOperator(task_id='clean-dm_all_indicators_v',
                                  sql="alter table public.dm_all_indicators_v truncate partition for(%s)",
                                  parameters=(int(load_id),),
                                  postgres_conn_id=postgres_conn_id,
                                  retries=30,
                                  retry_delay=timedelta(minutes=1))
    clean_tasks.append(clean_task)
    clean_task = PostgresOperator(task_id='clean-dm_all_indicators_t',
                                  sql="truncate table public.dm_all_indicators_t",
                                  postgres_conn_id=postgres_conn_id,
                                  retries=30,
                                  retry_delay=timedelta(minutes=1))
    clean_tasks.append(clean_task)
    with open(load_dds_script_path, 'r', encoding='utf-8') as f:
                script_body = f.read().encode('ascii', 'ignore')
    load_dds = PostgresOperator(task_id='load-dm_all_indicators_v',
                                sql=script_body,
                                parameters=(int(load_id),),
                                postgres_conn_id=postgres_conn_id,
                                retries=30,
                                retry_delay=timedelta(minutes=1))
    load_tasks = []
    script_files = [os.path.join(script_folder, file)
                    for file in os.listdir(script_folder) if file.endswith('.sql') and file[:3].isdigit()]
    script_files.sort()
    for i, script_file in enumerate(script_files):
        with open(script_file, 'r', encoding='utf-8') as f:
            script_body = f.read().encode('ascii', 'ignore')
            task = PostgresOperator(task_id=script_file.split(os.sep)[-1].replace('.sql', ''),
                                    sql=script_body,
                                    postgres_conn_id=postgres_conn_id,
                                    retries=30,
                                    retry_delay=timedelta(minutes=1))
            load_tasks.append(task)

    finish = DummyOperator(task_id='rzd-gp-finish',
                           retries=30,
                           retry_delay=timedelta(minutes=1))

    start >> clean_tasks >> load_dds >> load_tasks >> finish
