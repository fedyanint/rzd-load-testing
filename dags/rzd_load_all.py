from airflow import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils import dates
from git.rzd_load_hive import dag as dag_hive
from git.rzd_load_data import gp_dag
from git.rzd_load_arch import dag as dag_arch
from airflow.models import Variable
from datetime import timedelta


dag_params = {
    'dag_id': 'rzd-load-all',
    'start_date': dates.days_ago(1),
    'schedule_interval': None
}

retries = int(Variable.get('RETRIES', default_var=1)) or 1
retry_delay = timedelta(minutes=int(Variable.get('RETRY_DELAY_MINUTES', default_var=5))) or timedelta(minutes=5)

dag_hive.dag_id = 'rzd-load-all.load-hive'
gp_dag.dag_id = 'rzd-load-all.load-gp'
dag_arch.dag_id = 'rzd-load-all.load-arch'

with DAG(**dag_params) as all_dag:
    start = DummyOperator(task_id='load-all-start')
    
    load_arch_task = SubDagOperator(subdag=dag_arch, task_id='load-arch', retries=retries, retry_delay=retry_delay)
    load_hive_task = SubDagOperator(subdag=dag_hive, task_id='load-hive', retries=retries, retry_delay=retry_delay)
    load_gp_task = SubDagOperator(subdag=gp_dag, task_id='load-gp', retries=retries, retry_delay=retry_delay)
    
    finish = DummyOperator(task_id='load-all-finish')
    start >> load_hive_task >> load_gp_task >> finish
    start >> load_arch_task >> finish
