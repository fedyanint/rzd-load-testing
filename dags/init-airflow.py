import logging
from datetime import datetime, timedelta

from airflow import settings, DAG, AirflowException
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.hooks.base_hook import BaseHook
from airflow.models import Connection
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import PythonOperator

greenplum_connection_name = 'rzd_postgres_connect'
hadoop_ssh_connection_name = 'rzd_hadoop_connect'


def check_connection_existence(name):
    BaseHook.get_connection(name)
    logging.error("There is already connection with name {}".format(name))
    raise NameError("{} connection already exists".format(name))


def init_connection_to_greenplum():
    try:
        check_connection_existence(greenplum_connection_name)
    except AirflowException:
        logging.info('Create new connection {}'.format(greenplum_connection_name))
        conn = Connection(
            conn_id=greenplum_connection_name,
            conn_type='postgres',
            host='10.20.121.17',
            login='adb_admin',
            password='adb_admin',
            schema='postgres',
            port=5432
        )
        session = settings.Session()
        session.add(conn)
        session.commit()


def init_ssh_connection_to_hadoop():
    try:
        check_connection_existence(hadoop_ssh_connection_name)
    except AirflowException:
        logging.info('Create new connection {}'.format(hadoop_ssh_connection_name))
        conn = Connection(
            conn_id=hadoop_ssh_connection_name,
            conn_type='ssh',
            host='10.20.121.23',
            login='root',
            password='12345'
        )
        session = settings.Session()
        session.add(conn)
        session.commit()


default_args = {
    'owner': 'Airflow',
    'depends_on_past': False,
    'start_date': datetime.now() - timedelta(1),
    'email': ['tfedyanin@phoenixit.ru'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG('airflow-init',
          default_args=default_args,
          schedule_interval=None,
          catchup=False)

add_greenplum_connection = PythonOperator(
    task_id='add_greenplum_connection',
    provide_context=False,
    python_callable=init_connection_to_greenplum,
    dag=dag
)

test_greenplum_connection = PostgresOperator(
    task_id='test_greenplum_connection',
    postgres_conn_id=greenplum_connection_name,
    sql='SELECT 1',
    dag=dag
)

add_greenplum_connection >> test_greenplum_connection

add_hadoop_ssh_connection = PythonOperator(
    task_id='add_hadoop_ssh_connection',
    python_callable=init_ssh_connection_to_hadoop,
    dag=dag
)

test_hadoop_ssh_connection = SSHOperator(
    task_id='test_hadoop_ssh_connection',
    ssh_conn_id=hadoop_ssh_connection_name,
    command='uptime',
    dag=dag
)

add_hadoop_ssh_connection >> test_hadoop_ssh_connection

