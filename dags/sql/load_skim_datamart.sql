ALTER TABLE ods_skim_datamart DROP IF EXISTS PARTITION(load_id={{var.value.LOAD_ID}});

INSERT  INTO ods_skim_datamart PARTITION(load_id={{var.value.LOAD_ID}})
SELECT
	CURRENT_TIMESTAMP AS load_date,
	from_utc_timestamp(cal_date,'MSK') as cal_date,
	munit,
	var,
	value,
	source,
	org,
	data_type,
	val_type,
	metric_type,
	date_type,
	cargo_type,
	reason_text,
	from_utc_timestamp(mod_time,'MSK') as mod_time,
	user_login
FROM 
	stg_skim_datamart;

ALTER TABLE bv_skim_datamart DROP IF EXISTS PARTITION(load_id={{var.value.LOAD_ID}});

INSERT  INTO bv_skim_datamart PARTITION(load_id={{var.value.LOAD_ID}})
SELECT load_date
		, cal_date
		, munit
		, var
		, value
		, `source`
		, org
		, data_type
		, val_type
		, metric_type
		, date_type
		, cargo_type
		, reason_text
		, mod_time
		, user_login
FROM
    ods_skim_datamart;
	