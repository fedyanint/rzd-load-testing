/* 1. Отклонение факта к плану, % */

insert into public.dm_all_indicators_t(load_date, cal_date, munit, var, value, "source", org, data_type, val_type,
                                       metric_type, date_type, cargo_type, reason_text, mod_time, user_login, load_id,
                                       md5, hcode_id)
select load_date, cal_date, munit, var, value, "source", org, data_type, val_type, metric_type, date_type, cargo_type, 
       reason_text, mod_time, user_login, load_id, md5(row(cal_date, hcode_id)::text)::uuid, hcode_id
from (
select current_timestamp as load_date
     , vf.cal_date
     , vf.munit
     , vf.var
     , round(((vf.value - vp.value) / abs(vp.value)) * 100, 5) as value
     , vf.source
     , vf.org
     , vf.data_type
     , vf.val_type
     , cast('22' as varchar(2)) as metric_type /* Отклонение факта к плану, % */
     , vf.date_type
     , vf.cargo_type
     , vf.reason_text
     , vf.mod_time
     , vf.user_login
     , vf.load_id
     , cast('010' as varchar(3)) as hcode_id
  from public.dm_all_indicators_v vf
  left join public.dm_all_indicators_v vp 
    on vf.org        = vp.org
   and vf.cargo_type = vp.cargo_type
   and vf.munit      = vp.munit
   and vf.cal_date   = vp.cal_date
   and vp.metric_type in ('12')     /* План */
   and vf.val_type   = vp.val_type  /* Итог */
   and vf.date_type  = vp.date_type /* Сутки */
   -- and vf.hcode_id = vp.hcode_id
where vf.val_type = '1'        /* Итог */
  and vf.date_type = '4'       /* Сутки */
  and vf.metric_type in ('17') /* Факт */
  ) calc