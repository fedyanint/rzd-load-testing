/* 2. Нарастающий итог по месяцам с начала квартала для факта */

insert into public.dm_all_indicators_t(load_date, cal_date, munit, var, value, "source", org, data_type, val_type,
                                       metric_type, date_type, cargo_type, mod_time, load_id,
                                       md5, hcode_id)
select current_timestamp as load_date, cal_date, munit, var, value, "source", org, data_type, val_type, metric_type, 
       date_type, cargo_type, mod_time, load_id, md5(row(cal_date, hcode_id)::text)::uuid, hcode_id
from (
select distinct 
     --, vf.cal_date
     date_trunc('month',cal_date) as cal_date
     , vf.munit
     , vf.var
     , sum(value) over (partition by org, vf.munit, vf.var, vf."source", vf.org, vf.data_type, vf.val_type, 
                                          vf.metric_type, vf.date_type, vf.cargo_type, vf.mod_time, vf.load_id, 
                                          to_char(vf.cal_date, 'YYYY-Q')
                        order by date_trunc('month',cal_date)) as value
     , vf.source
     , vf.org
     , vf.data_type
     , cast('10' as varchar(2)) as val_type /* Нарастающий итог по месяцам с начала квартала */
     , vf.metric_type
     , vf.date_type
     , vf.cargo_type
     , vf.mod_time
     , vf.load_id
     , cast('020' as varchar(3)) as hcode_id
  from public.dm_all_indicators_v vf 
 where vf.val_type = '1'        /* Итог */
   and vf.date_type = '2'       /* Месяц */
   and vf.metric_type in ('17') /* Факт */
  /* and to_char(vf.cal_date, 'DD') = '01' */
--and vf.org=-1028031 and vf.munit='70' and vf.source='source3'
) calc