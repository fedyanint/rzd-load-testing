insert into dm_all_indicators_stg
select load_date
     , case date_type
       when 1 then date_trunc('year', cal_date)
       when 2 then date_trunc('month', cal_date)
       when 3 then date_trunc('week', cal_date)
       when 4 then date_trunc('day', cal_date)
       when 5 then date_trunc('quarter', cal_date)
       else cal_date end as cal_date
     , munit
     , var
     , value
     , "source"
     , org
     , data_type
     , val_type
     , metric_type
     , date_type
     , cargo_type
     , reason_text
     , mod_time
     , user_login
     , load_id
     , md5(row(cal_date, var, val_type, metric_type, date_type, org, cargo_type, mod_time)::text)::uuid
     , cast('000' as varchar(5)) as hcode_id
  from bv_skim_datamart osm
 where load_id = %s;


insert into dm_all_indicators_v
select * from dm_all_indicators_stg;