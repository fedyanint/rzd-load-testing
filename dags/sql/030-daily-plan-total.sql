/* 3. Итог за сутки для плана */

insert into public.dm_all_indicators_t(load_date, cal_date, munit, var, value, "source", org, data_type, val_type, 
                                       metric_type, date_type, cargo_type, mod_time, load_id, 
                                       md5, hcode_id)
select load_date, cal_date, munit, var, value, "source", org, data_type, val_type, metric_type, date_type, cargo_type, 
       mod_time, load_id, md5(row(cal_date, hcode_id)::text)::uuid, hcode_id
from (
with days as (select calendar_date,date_trunc('month', calendar_date) as beg_mnth
              from (select generate_series((date '2000-01-01')::timestamp, 
                                           (date '2040-12-31')::timestamp,
                                           interval '1 day'))
                                           sq1(calendar_date)) 
select current_timestamp as load_date
     ,sq2.calendar_date as cal_date
     , vf.munit
     , vf.var
     , round((vf.value / extract(days from vf.cal_date + INTERVAL '1 month' - vf.cal_date))::numeric, 5) as value
     , vf.source
     , vf.org
     , vf.data_type
     , cast('1' as varchar(1)) as val_type /* Итог */
     , vf.metric_type
     , 4 as date_type /* Сутки */
     , vf.cargo_type
     , vf.mod_time
     , vf.load_id
     , cast('030' as varchar(3)) as hcode_id
from public.dm_all_indicators_v vf 
inner join days sq2 on sq2.beg_mnth = date_trunc('month', cal_date)
 where vf.val_type  = '1'       /* Итог */
   and vf.date_type = '2'       /* Месяц */
   and vf.metric_type in ('12') /* План */
--and vf.org=-1027937 --and vf.munit='61' and vf.source='source12'
) calc