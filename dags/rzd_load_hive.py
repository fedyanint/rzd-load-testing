import os
from airflow import DAG
from airflow.operators import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.models import Variable
from datetime import datetime, timedelta

# Following are defaults which can be overridden later on
default_args = {
    'start_date': datetime(2020, 2, 4),
    'schedule_interval': None
}
hive_conn_id='rzd_hive_connect'
ssh_conn_id='rzd_hadoop_connect'
LOAD_ID = Variable.get('LOAD_ID', default_var='0')

dag = DAG('rzd_load_hive', schedule_interval=None, default_args=default_args)

start = DummyOperator(
        task_id='start',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

clean_xml = SSHOperator(
        command="hadoop fs -rm -r -f -skipTrash /user/admin/stg_xml/*",
        ssh_conn_id=ssh_conn_id,
        task_id="clean_stg_xml",
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))
        
load_xml = SSHOperator(
        command='hadoop fs -cp -f /user/admin/gen_xml/{0}.xml /user/admin/stg_xml/'.format(LOAD_ID.zfill(3)),
        ssh_conn_id=ssh_conn_id,
        task_id="load_xml",
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

hql_path = os.path.join(os.path.dirname(__file__), 'sql/load_skim_datamart.sql')   

hql = open(hql_path).read()


load_ods = SSHOperator(
        command='hive -e "{0}"'.format(hql),
        ssh_conn_id=ssh_conn_id,
        task_id="load_ods",
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

#load_ods_skim_datamart = HiveOperator(
#    hql='sql/load_skim_datamart.sql',
#    params={ 'LOAD_ID': LOAD_ID},
#    hive_cli_conn_id=hive_conn_id,
#    schema='default',
#    hiveconf_jinja_translate=True,
#    task_id='load_ods',
#    dag=dag)

end = DummyOperator(
        task_id='end',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

#start >> load_ods >> end
start >>  clean_xml >> load_xml >> load_ods >> end