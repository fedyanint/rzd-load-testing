import os
from airflow import DAG
from airflow.operators import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.models import Variable
from datetime import datetime, timedelta

# Following are defaults which can be overridden later on
default_args = {
    'start_date': datetime(2020, 2, 4),
    'schedule_interval': None
}

ssh_conn_id='rzd_hadoop_connect'

LOAD_ID = Variable.get('LOAD_ID', default_var='0').zfill(3)

dag = DAG('rzd_load_arch', schedule_interval=None, default_args=default_args)

start = DummyOperator(
        task_id='start',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

clean_arch = SSHOperator(
        command='hadoop fs -rm -r -f -skipTrash /user/admin/arch_xml/{0}.gzip'.format(LOAD_ID),
        ssh_conn_id=ssh_conn_id,
        task_id='clean_arch',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))
                
create_arch = SSHOperator(
        command='hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar -Dmapred.output.compress=true -Dmapred.compress.map.output=true -Dmapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec -Dmapred.reduce.tasks=0 -input /user/admin/gen_xml/{0}.xml -output /user/admin/arch_xml/{0}.gzip'.format(LOAD_ID),
        ssh_conn_id=ssh_conn_id,
        task_id='create_arch',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))
        
        

        

end = DummyOperator(
        task_id='end',
        dag=dag,
        retries=30,
        retry_delay=timedelta(minutes=1))

start >>  clean_arch >> create_arch >> end
